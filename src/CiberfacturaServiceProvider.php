<?php

namespace Codelko\Ciberfactura;

use Illuminate\Support\ServiceProvider;
use Codelko\Ciberfactura\Libraries\Cfdi;

class CiberfacturaServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/migrations' => base_path('database/migrations'),
            __DIR__.'/config' => base_path('config/packages/codelko/ciberfactura'),
        ]);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('cfdi', function () {
            return new Cfdi();
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [
            'cfdi'
        ];
    }
}
