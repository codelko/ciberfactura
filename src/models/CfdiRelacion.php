<?php
namespace Codelko\Ciberfactura\Models;

use \Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;
use Codelko\Ciberfactura\Libraries\CfdiException;

class CfdiRelacion extends Model{
    protected $table = "cfdi_v33_relaciones";

    protected $fillable = ['cfdi_id','tipo_relacion','uuid'];

    protected static $rules = [
        "cfdi_id" => "required",
        "tipo_relacion" => "required",
        "uuid" => "required"
    ];

    protected static $messages = [
        'cfdi_id.required' => 'El CFDI al que pertenece el CFDI es obligatorio.',
        'tipo_relacion.required' => 'El Tipo de Relacion del CFDI es obligatoria.',
        'uuid.required' => 'El UUID del CFDI relacionado es obligatorio.',
    ];

    public static function validate($data){
        $validator = Validator::make($data, CfdiRelacion::$rules, CfdiRelacion::$messages);

        if ($validator->fails()) {
            $errors = $validator->errors()->getMessages();

            if($errors){
                foreach($errors as $error){
                    throw new CfdiException($error[0]);
                }
            }

            return false;
        }

        return true;
    }

    public function cfdi(){
        return $this->belongsTo('Codelko\Ciberfactura\Models\CfdiFactura', 'cfdi_id');
    }
}