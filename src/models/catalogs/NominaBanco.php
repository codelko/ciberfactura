<?php
namespace Codelko\Ciberfactura\Models\Catalogs;

use \Illuminate\Database\Eloquent\Model;

class NominaBanco extends Model{
    protected $table = "cfdi_nomina_v12_cat_bancos";

    protected $fillable = ['code', 'name'];
}