<?php
namespace Codelko\Ciberfactura\Models\Catalogs;

use \Illuminate\Database\Eloquent\Model;

class NominaIncapacidad extends Model{
    protected $table = "cfdi_nomina_v12_cat_incapacidades";

    protected $fillable = ['code', 'name'];
}