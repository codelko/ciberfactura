<?php
namespace Codelko\Ciberfactura\Models\Catalogs;

use \Illuminate\Database\Eloquent\Model;

class CfdiSegmentoProducto extends Model{
    protected $table = "cfdi_v33_cat_segmentos_productos_servicios";

    protected $fillable = ['id','type_id', 'name'];

}