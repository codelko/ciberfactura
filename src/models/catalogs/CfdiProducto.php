<?php
namespace Codelko\Ciberfactura\Models\Catalogs;

use \Illuminate\Database\Eloquent\Model;

class CfdiProducto extends Model{
    protected $table = "cfdi_v33_cat_productos_servicios";

    protected $fillable = ['id','type_id','segment_id','family_id','class_id','code', 'name'];

    public function type(){
        return $this->belongsTo('Codelko\Ciberfactura\Models\Catalogs\CfdiTipoProducto');
    }

    public function segment(){
        return $this->belongsTo('Codelko\Ciberfactura\Models\Catalogs\CfdiSegmentoProducto');
    }

    public function family(){
        return $this->belongsTo('Codelko\Ciberfactura\Models\Catalogs\CfdiFamiliaProducto');
    }

    public function clase(){
        return $this->belongsTo('Codelko\Ciberfactura\Models\Catalogs\CfdiClaseProducto');
    }
}