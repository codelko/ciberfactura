<?php
namespace Codelko\Ciberfactura\Models\Catalogs;

use \Illuminate\Database\Eloquent\Model;

class CfdiImpuesto extends Model{
    protected $table = "cfdi_v33_cat_impuestos";

    protected $fillable = ['code', 'name', 'traslado', 'retencion'];
}