<?php
namespace Codelko\Ciberfactura\Models\Catalogs;

use \Illuminate\Database\Eloquent\Model;

class CfdiClaseProducto extends Model{
    protected $table = "cfdi_v33_cat_clases_productos_servicios";

    protected $fillable = ['id', 'type_id', 'segment_id', 'family_id', 'name'];

}