<?php
namespace Codelko\Ciberfactura\Models\Catalogs;

use \Illuminate\Database\Eloquent\Model;

class CfdiServicio extends Model{
    protected $table = "cfdi_v33_cat_productos_servicios";

    protected $fillable = ['type','segment','family','class','code', 'name'];

    public function type(){
        return $this->belongsTo('Codelko\Ciberfactura\Models\Catalogs\CfdiTipoProducto');
    }

    public function segment(){
        return $this->belongsTo('Codelko\Ciberfactura\Models\Catalogs\CfdiSegmentoProducto');
    }

    public function family(){
        return $this->belongsTo('Codelko\Ciberfactura\Models\Catalogs\CfdiFamiliaProducto');
    }

    public function clase(){
        return $this->belongsTo('Codelko\Ciberfactura\Models\Catalogs\CfdiClaseProducto');
    }
}