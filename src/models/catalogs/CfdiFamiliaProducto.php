<?php
namespace Codelko\Ciberfactura\Models\Catalogs;

use \Illuminate\Database\Eloquent\Model;

class CfdiFamiliaProducto extends Model{
    protected $table = "cfdi_v33_cat_familias_productos_servicios";

    protected $fillable = ['id', 'type_id', 'segment_id', 'name'];

}