<?php
namespace Codelko\Ciberfactura\Models\Catalogs;

use \Illuminate\Database\Eloquent\Model;

class CfdiTipoProducto extends Model{
    protected $table = "cfdi_v33_cat_tipos_productos_servicios";

    protected $fillable = ['id', 'name'];

}