<?php
namespace Codelko\Ciberfactura\Models\Catalogs;

use \Illuminate\Database\Eloquent\Model;

class NominaRiesgo extends Model{
    protected $table = "cfdi_nomina_v12_cat_riesgos";

    protected $fillable = ['code', 'name'];
}