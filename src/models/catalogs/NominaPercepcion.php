<?php
namespace Codelko\Ciberfactura\Models\Catalogs;

use \Illuminate\Database\Eloquent\Model;

class NominaPercepcion extends Model{
    protected $table = "cfdi_nomina_v12_cat_percepciones";

    protected $fillable = ['code', 'name'];
}