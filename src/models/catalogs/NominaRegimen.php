<?php
namespace Codelko\Ciberfactura\Models\Catalogs;

use \Illuminate\Database\Eloquent\Model;

class NominaRegimen extends Model{
    protected $table = "cfdi_nomina_v12_cat_regimenes";

    protected $fillable = ['code', 'name'];
}