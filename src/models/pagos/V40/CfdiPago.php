<?php
namespace Codelko\Ciberfactura\Models\Pagos\V40;

use \Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;
use Codelko\Ciberfactura\Libraries\CfdiException;

class CfdiPago extends Model{
    protected $table = "cfdi_v40_pagos";

    protected $guarded = [];
}