<?php
namespace Codelko\Ciberfactura\Models\Pagos\V40;

use \Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;
use Codelko\Ciberfactura\Libraries\CfdiException;

class CfdiPagoRelacion extends Model{
    protected $table = "cfdi_v40_pagos_relaciones";

    protected $guarded = [];
}