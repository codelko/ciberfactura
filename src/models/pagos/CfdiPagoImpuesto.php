<?php
namespace Codelko\Ciberfactura\Models\Pagos;

use \Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;
use Codelko\Ciberfactura\Libraries\CfdiException;

class CfdiPagoImpuesto extends Model{
    protected $table = "cfdi_v33_pagos_impuestos";

    protected $fillable = ['cfdi_id','cfdi_pago_id', 'type','impuesto','tipo_factor','tasa_o_cuota','importe'];

    protected static $rules = [
        "cfdi_id" => "required",
        "cfdi_pago_id" => "required",
        "type" => "required",
        "impuesto" => "required",
        'importe' => 'required'
    ];

    protected static $messages = [
        'cfdi_id.required' => 'El Cfdi al que pertenece el pago es obligatorio.',
        'cfdi_pago_id.required' => 'El pago al que pertenece el impuesto es obligatorio.',
        'type.required' => 'El tipo del impuesto es obligatorio.',
        'impuesto.required' => 'La clave del impuesto es obligatorio.',
        'importe.required' => 'El importe del impuesto es obligatorio.',
    ];

    public function cfdi(){
        return $this->belongsTo('Codelko\Ciberfactura\Models\CfdiFactura', 'cfdi_id');
    }

    public function pago(){
        return $this->belongsTo('Codelko\Ciberfactura\Models\CfdiPago', 'cfdi_pago_id');
    }

    public static function validate($data){
        $validator = Validator::make($data, CfdiPagoImpuesto::$rules, CfdiPagoImpuesto::$messages);

        if ($validator->fails()) {
            $errors = $validator->errors()->getMessages();

            if($errors){
                foreach($errors as $error){
                    throw new CfdiException($error[0]);
                }
            }

            return false;
        }

        return true;
    }
}