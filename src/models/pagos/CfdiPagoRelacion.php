<?php
namespace Codelko\Ciberfactura\Models\Pagos;

use \Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;
use Codelko\Ciberfactura\Libraries\CfdiException;

class CfdiPagoRelacion extends Model{
    protected $table = "cfdi_v33_pagos_relaciones";

    protected $fillable = ['cfdi_id','cfdi_pago_id','id_documento','serie','folio','moneda','tipo_cambio','metodo_pago','num_parcialidad','importe_saldo_anterior','importe_pagado','importe_saldo_insoluto'];

    protected static $rules = [
        "cfdi_id" => "required",
        "cfdi_pago_id" => "required",
        "id_documento" => "required",
        "moneda" => "required",
        'metodo_pago' => 'required'
    ];

    protected static $messages = [
        'cfdi_id.required' => 'El Cfdi al que pertenece el pago es obligatorio.',
        'cfdi_pago_id.required' => 'El pago al que pertenece el impuesto es obligatorio.',
        'id_documento.required' => 'El cfdi al que se acredita el pago es obligatorio.',
        'moneda.required' => 'La moneda del cfdi al que acredita el pago es obligatorio.',
        'metodo_pago.required' => 'El método de pago del cfdi al que acredita el pago es obligatorio.',
    ];

    public function cfdi(){
        return $this->belongsTo('Codelko\Ciberfactura\Models\CfdiFactura', 'cfdi_id');
    }

    public function pago(){
        return $this->belongsTo('Codelko\Ciberfactura\Models\CfdiPago', 'cfdi_pago_id');
    }

    public static function validate($data){
        $validator = Validator::make($data, CfdiPagoRelacion::$rules, CfdiPagoRelacion::$messages);

        if ($validator->fails()) {
            $errors = $validator->errors()->getMessages();

            if($errors){
                foreach($errors as $error){
                    throw new CfdiException($error[0]);
                }
            }

            return false;
        }

        return true;
    }
}