<?php
namespace Codelko\Ciberfactura\Models\Pagos;

use \Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;
use Codelko\Ciberfactura\Libraries\CfdiException;

class CfdiPago extends Model{
    protected $table = "cfdi_v33_pagos";

    protected $fillable = ['cfdi_id','fecha_pago','forma_pago','moneda','tipo_cambio','monto','num_operacion','rfc_emisor_cta_ord','nom_banco_ord_ext','cta_ordenante','rfc_emisor_cta_ben','cta_beneficiario','tipo_cad_pago', 'cert_pago', 'cad_pago', 'sello_pago'];

    protected static $rules = [
        "cfdi_id" => "required",
        "fecha_pago" => "required",
        "forma_pago" => "required",
        "moneda" => "required",
        "monto" => "required"
    ];

    protected static $messages = [
        'cfdi_id.required' => 'El cfdi del pago es obligatorio.',
        'fecha.required' => 'La fecha del pago es obligatoria.',
        'forma_pago.required' => 'La forma de pago es obligatoria.',
        'moneda.required' => 'La moneda del pago es obligatoria.',
        'monto.required' => 'El monto del pago es obligatorio.'
    ];

    public function cfdi(){
        return $this->belongsTo('Codelko\Ciberfactura\Models\CfdiFactura', 'cfdi_id');
    }

    public function relaciones(){
        return $this->hasMany('Codelko\Ciberfactura\Models\Pagos\CfdiPagoRelacion', 'cfdi_pago_id');
    }

    public function impuestos(){
        return $this->hasMany('Codelko\Ciberfactura\Models\Pagos\CfdiPagoImpuesto', 'cfdi_pago_id');
    }

    public static function validate($data){
        $validator = Validator::make($data, CfdiPago::$rules, CfdiPago::$messages);

        if ($validator->fails()) {
            $errors = $validator->errors()->getMessages();

            if($errors){
                foreach($errors as $error){
                    throw new CfdiException($error[0]);
                }
            }

            return false;
        }

        return true;
    }
}