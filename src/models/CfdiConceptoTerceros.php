<?php
namespace Codelko\Ciberfactura\Models;

use \Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;
use Codelko\Ciberfactura\Libraries\CfdiException;

class CfdiConceptoTerceros extends Model{
    protected $table = "cfdi_v33_concepto_terceros";

    protected $fillable = ['cfdi_id','cfdi_concepto_id','version','rfc','nombre','calle','no_exterior','no_interior','colonia','localidad','municipio','estado','pais','codigo_postal','cuenta_predial'];

    protected static $rules = [
        "cfdi_id" => "required",
        "cfdi_concepto_id" => "required",
        "version" => "required",
        "rfc" => "required",
        "nombre" => "required",
    ];

    protected static $messages = [
        'cfdi_id.required' => 'El Cfdi al que pertenece el Concepto de Terceros es obligatorio.',
        'cfdi_concepto_id.required' => 'El Concepto al que pertenece el Concepto de Terceros es obligatorio.',
        'version.required' => 'La versión del complemento de terceros es obligatorio.',
        'rfc.required' => 'El rfc del tercero en el concepto es obligatorio.',
        'nombre.required' => 'El nombre del tercero en el concepto es obligatorio.'
    ];

    public static function validate($data){
        $validator = Validator::make($data, CfdiConcepto::$rules, CfdiConcepto::$messages);

        if ($validator->fails()) {
            $errors = $validator->errors()->getMessages();

            if($errors){
                foreach($errors as $error){
                    throw new CfdiException($error[0]);
                }
            }

            return false;
        }

        return true;
    }

    public function addImpuesto($data){
        if(!isset($data['cfdi_id'])){
            $data['cfdi_id'] = $this->cfdi_id;
        }

        if(!isset($data['cfdi_concepto_id'])){
            $data['cfdi_concepto_id'] = $this->cfdi_concepto_id;
        }

        if(!isset($data['cfdi_concepto_terceros_id'])){
            $data['cfdi_concepto_terceros_id'] = $this->id;
        }

        $cfdi_impuesto = CfdiImpuestoTerceros::create($data);

        return $cfdi_impuesto;
    }

    public function impuestos(){
        return $this->hasMany('Codelko\Ciberfactura\Models\CfdiImpuestoTerceros', 'cfdi_concepto_terceros_id');
    }
}