<?php namespace Codelko\Ciberfactura\Models\V40;

use Illuminate\Database\Eloquent\Model;

class CfdiReceptor extends Model {

    protected $table = 'cfdi_v40_receptores';
    protected $guarded = [];

}