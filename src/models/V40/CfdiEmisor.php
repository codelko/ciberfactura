<?php namespace Codelko\Ciberfactura\Models\V40;

use Illuminate\Database\Eloquent\Model;

class CfdiEmisor extends Model {

    protected $table = 'cfdi_v40_emisores';
    protected $guarded = [];

}