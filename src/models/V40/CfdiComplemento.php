<?php namespace Codelko\Ciberfactura\Models\V40;

use Illuminate\Database\Eloquent\Model;

class CfdiComplemento extends Model {

    protected $table = 'cfdi_v40_complementos';
    protected $guarded = [];

}