<?php namespace Codelko\Ciberfactura\Models\V40;

use Illuminate\Database\Eloquent\Model;

class CfdiRelacion extends Model {

    protected $table = 'cfdi_v40_relaciones';
    protected $guarded = [];

}