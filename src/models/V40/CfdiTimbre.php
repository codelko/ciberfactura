<?php namespace Codelko\Ciberfactura\Models\V40;

use Illuminate\Database\Eloquent\Model;

class CfdiTimbre extends Model {

    protected $table = 'cfdi_v40_timbres';
    protected $guarded = [];

}