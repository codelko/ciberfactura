<?php namespace Codelko\Ciberfactura\Models\V40;

use Illuminate\Database\Eloquent\Model;

class CfdiFactura extends Model {

    protected $table = 'cfdi_v40_facturas';
    protected $guarded = [];

    public function emisor(){
        return $this->hasOne('Codelko\Ciberfactura\Models\V40\CfdiEmisor', 'cfdi_id');
    }

    public function receptor(){
        return $this->hasOne('Codelko\Ciberfactura\Models\V40\CfdiReceptor', 'cfdi_id');
    }

    public function conceptos(){
        return $this->hasMany('Codelko\Ciberfactura\Models\V40\CfdiConcepto', 'cfdi_id');
    }

    public function impuestos(){
        return $this->hasMany('Codelko\Ciberfactura\Models\V40\CfdiImpuesto', 'cfdi_id');
    }

    public function relaciones(){
        return $this->hasMany('Codelko\Ciberfactura\Models\V40\CfdiRelacion', 'cfdi_id');
    }

    public function timbre(){
        return $this->hasOne('Codelko\Ciberfactura\Models\V40\CfdiTimbre', 'cfdi_id');
    }

    public function complementos(){
        return $this->hasMany('Codelko\Ciberfactura\Models\V40\CfdiComplemento', 'cfdi_id');
    }
}