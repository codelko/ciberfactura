<?php namespace Codelko\Ciberfactura\Models\V40;

use Illuminate\Database\Eloquent\Model;

class CfdiConcepto extends Model {

    protected $table = 'cfdi_v40_conceptos';
    protected $guarded = [];

    public function impuestos(){
        return $this->hasMany('Codelko\Ciberfactura\Models\V40\CfdiImpuesto', 'cfdi_concepto_id');
    }

    public function terceros(){
        return $this->hasMany('Codelko\Ciberfactura\Models\V40\CfdiTerceros', 'cfdi_concepto_id');
    }
}