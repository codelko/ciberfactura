<?php namespace Codelko\Ciberfactura\Models\V40;

use Illuminate\Database\Eloquent\Model;

class CfdiImpuesto extends Model {

    protected $table = 'cfdi_v40_impuestos';
    protected $guarded = [];

}