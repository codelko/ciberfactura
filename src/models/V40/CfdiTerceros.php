<?php namespace Codelko\Ciberfactura\Models\V40;

use Illuminate\Database\Eloquent\Model;

class CfdiTerceros extends Model {

    protected $table = 'cfdi_v40_terceros';
    protected $guarded = [];

}