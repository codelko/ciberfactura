<?php
namespace Codelko\Ciberfactura\Models;

use \Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;
use Codelko\Ciberfactura\Libraries\CfdiException;

class CfdiComplemento extends Model{
    protected $table = "cfdi_v33_complementos";

    protected $fillable = ['cfdi_id','name','xml'];

    protected static $rules = [
        "cfdi_id" => "required",
        "name" => "required",
        "xml" => "required"
    ];

    protected static $messages = [
        'cfdi_id.required' => 'El Cfdi al que pertenece el Timbre es obligatorio.',
        'name.required' => 'El nombre del complemento es obligatorio.',
        'xml.required' => 'El XML del complemento es obligatorio.'
    ];

    public static function validate($data){
        $validator = Validator::make($data, CfdiComplemento::$rules, CfdiComplemento::$messages);

        if ($validator->fails()) {
            $errors = $validator->errors()->getMessages();

            if($errors){
                foreach($errors as $error){
                    throw new CfdiException($error[0]);
                }
            }

            return false;
        }

        return true;
    }
}