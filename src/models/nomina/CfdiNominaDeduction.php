<?php
namespace Codelko\Ciberfactura\Models\Nomina;

use \Illuminate\Database\Eloquent\Model;

class CfdiNominaDeduction extends Model{
    protected $table = "cfdi_nomina_v12_deducciones";

    protected $fillable = [
        "id",
        "account_id",
        "cfdi_id",
        "nomina_id",
        "tipo",
        "clave",
        "concepto",
        "importe_gravado",
        "importe_exento"
    ];
}