<?php
namespace Codelko\Ciberfactura\Models\Nomina;

use \Illuminate\Database\Eloquent\Model;

class CfdiNominaPerception extends Model{
    protected $table = "cfdi_nomina_v12_percepciones";

    protected $fillable = [
        "id",
        "account_id",
        "cfdi_id",
        "nomina_id",
        "tipo",
        "clave",
        "concepto",
        "importe_gravado",
        "importe_exento"
    ];
}