<?php
namespace Codelko\Ciberfactura\Models\Nomina;

use \Illuminate\Database\Eloquent\Model;

class CfdiNomina extends Model{
    protected $table = "cfdi_nomina_v12_nominas";

    protected $fillable = [
        "id",
        "account_id",
        "cfdi_id",
        "employee_id",
        "employer_id",
        "version",
        "registro_patronal",
        "curp_patronal",
        "numero_empleado",
        "rfc",
        "curp",
        "tipo_regimen",
        "nss",
        "fecha_pago",
        "fecha_inicial",
        "fecha_final",
        "dias_trabajados",
        "clabe",
        "banco",
        "fecha_contratacion",
        "antiguedad",
        "puesto",
        "tipo_contrato",
        "tipo_jornada",
        "periodicidad",
        "riesgo_puesto",
        "salario_base",
        "salario_integrado",
        "outsourcing",
        "estado",
        "xml",
        "pdf"
    ];

    public function getTotalPercepcionesAttribute(){
        $total = 0;

        if($this->perceptions) foreach($this->perceptions as $perception){
            if(str_pad($perception->clave,3,"0", STR_PAD_LEFT) != '017' && str_pad($perception->clave,3,"0", STR_PAD_LEFT) != '050') {
                $total += $perception->importe_gravado + $perception->importe_exento;
            }
        }

        return $total;
    }

    public function getTotalDeduccionesAttribute(){
        $total = 0;

        if($this->deductions) foreach($this->deductions as $deductions){
            $total += $deductions->importe_gravado + $deductions->importe_exento;
        }

        return $total;
    }

    public function getTotalOtrosPagosAttribute(){
        $total = 0;

        $subsidio = false;
        if($this->perceptions) foreach($this->perceptions as $perception){
            if(str_pad($perception->clave,3,"0", STR_PAD_LEFT) == '017' || str_pad($perception->clave,3,"0", STR_PAD_LEFT) == '050') {
                $total += $perception->importe_gravado + $perception->importe_exento;
                $subsidio = true;
            }
        }

        if(!$subsidio){
            return $total + 0.01;
        }

        return $total;
    }

    public function cfdi(){
        return $this->belongsTo('Codelko\Ciberfactura\Models\CfdiFactura', 'cfdi_id', 'id');
    }

    public function perceptions(){
        return $this->hasMany('Codelko\Ciberfactura\Models\Nomina\CfdiNominaPerception', 'nomina_id', 'id');
    }

    public function deductions(){
        return $this->hasMany('Codelko\Ciberfactura\Models\Nomina\CfdiNominaDeduction', 'nomina_id', 'id');
    }
}