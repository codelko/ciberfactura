<?php
namespace Codelko\Ciberfactura\Libraries;

use Genkgo\Xsl\XsltProcessor;
use Illuminate\Support\Facades\Config;
use Codelko\Ciberfactura\Models\CfdiComplemento;
use Codelko\Ciberfactura\Models\CfdiFactura;
use Codelko\Ciberfactura\Models\CfdiTimbre;

class CfdiBase {
    public $xml;
    public $cadena_original;
    public $sello;
    public $key;
    public $certificado;
    public $no_certificado;
    public $tmp_file;
    public $cfdi;
    public $rfc;
    public $production = false;
    public $certificate = array();

    public $timbrador;
    public $cancelador;
    
    public $path = false;

    protected $version = "3.3";

    //Base Constructor
    public function __construct($cfdi = false, $certificate = false){
        $this->production = Config::get('packages.codelko.ciberfactura.config.production');

        $url_cer = str_replace("\\","/", base_path())."/".Config::get('packages.codelko.ciberfactura.config.certificate.sandbox.cer');
        $url_key = str_replace("\\","/", base_path())."/".Config::get('packages.codelko.ciberfactura.config.certificate.sandbox.key');
        $clave_privada = Config::get('packages.codelko.ciberfactura.config.certificate.sandbox.password');

        $this->no_certificado = CfdiBase::getSerialFromCertificate( $url_cer );

        $this->certificado = CfdiBase::getCertificate( $url_cer, false );
        $this->key = CfdiBase::getPrivateKey($url_key, $clave_privada);

        $this->rfc = Config::get('packages.codelko.ciberfactura.config.rfc');

        $this->certificate = [
            "cer" => $url_cer,
            "key" => $url_key,
            "password" => $clave_privada,
            "serial_number" => $this->no_certificado
        ];

        $this->timbrador = new CfdiTimbrador($this->rfc, $this->certificate);
        $this->cancelador = new CfdiCancelador($this->rfc, $this->certificate);

        if($cfdi){
            $this->load($cfdi);
        }

        if($certificate){
            $this->loadCertificate($certificate);
        }
    }

    public function setTimbrador(CfdiTimbradoInterface $timbrador){
        $this->timbrador = $timbrador;
    }

    //Load Params
    public function load($cfdi, $decimals = 2){
        $this->validate($cfdi);

        if(!$this->path){
            $this->path = public_path()."/cfdis";
        }

        $this->cfdi = $cfdi;
        $this->rfc = $cfdi->emisor->rfc;

        //Validación para descartar versiones de 3.2
        if($cfdi->version != "3.2"){
            if(!$this->cfdi->no_certificado){
                $this->cfdi->no_certificado = $this->no_certificado;
                $this->cfdi->certificado = $this->certificado;
            }

            $xslt = __DIR__.'/../resources/xslt/cadenaoriginal_3_3.xslt';

            if($cfdi->version == "4.0"){
                $xslt = __DIR__.'/../resources/xslt/cadenaoriginal_4_0.xslt';
            }

            if(!file_exists($xslt)){
                throw new CfdiException("El archivo xslt para necesario para formar la cadena original de la factura no existe en la ruta definida. [$xslt]");
            }

            if($cfdi->version == "4.0"){
                $this->xml = new CfdiV40Generator($this->cfdi, $decimals);
            }
            else{
                $this->xml = new CfdiGenerator($this->cfdi, $decimals); 
            }

            if(!file_exists(Config::get('packages.codelko.ciberfactura.config.path'))){
                mkdir(Config::get('packages.codelko.ciberfactura.config.path'));
            }

            if(!file_exists(public_path()."/temp")){
                mkdir(public_path()."/temp");
            }

            $this->tmp_file = public_path()."/temp/".strtoupper(sha1(date("Y-m-d H:i:s".rand(0,100000)))).".xml";
            $this->xml->saveFile($this->tmp_file, false);

            $this->cadena_original = CfdiBase::getOriginalString($this->tmp_file, $xslt);

            @unlink($this->tmp_file);

            $this->cfdi->cadena_original = trim(str_replace("\n","",  str_replace("\r","",  $this->cadena_original)));
            $this->cfdi->save();
        }
        else{
            if(!$this->cfdi->noCertificado){
                $this->cfdi->noCertificado = $this->no_certificado ? $this->no_certificado : '';
                $this->cfdi->certificado = $this->certificado;
            }

            $this->cfdi->cadenaOriginal = trim(str_replace("\n","",  str_replace("\r","",  $this->cadena_original)));
            $this->cfdi->save();
        }

        $this->timbrador = new CfdiTimbrador($this->rfc, $this->certificate);
        $this->cancelador = new CfdiCancelador($this->rfc, $this->certificate);
    }

    public function loadCertificate($certificate){
        $url_cer = $certificate["cer"];
        $url_key = $certificate["key"];
        $clave_privada = $certificate["password"];

        $this->no_certificado = $certificate["serial_number"];

        if(!$url_cer || !$url_key || !$clave_privada){
            throw new CfdiException("La configuración del certificado de sello digital no ha sido definida.");
        }

        if(!file_exists($url_cer) || !is_file($url_cer)){
            throw new CfdiException("El archivo (.cer) del Certificado de Sello Digital no fue encontrado. [$url_cer]");
        }

        if(!file_exists($url_key) || !is_file($url_key)){
            throw new CfdiException("El archivo (.key) del Certificado de Sello Digital no fue encontrado. [$url_key]");
        }

        $this->certificado = CfdiBase::getCertificate( $url_cer, false );
        $this->key = CfdiBase::getPrivateKey($url_key, $clave_privada);

        if($certificate){
            $this->certificate = $certificate;
        }
        else{
            $this->certificate = array(
                'cer' => $url_cer,
                'key' => $url_key,
                'password' => $clave_privada
            );
        }

        $certificate_number = CfdiBase::getSerialFromCertificate( $url_cer );

        if(!$certificate_number){
            throw new CfdiException("El Certificado de Sello Digital no es correcto.");
        }

        $this->no_certificado = $certificate_number;

        $certificate = CfdiBase::getCertificate( $url_cer, false );

        if(!$certificate){
            throw new CfdiException("El Certificado de Sello Digital (.cer) no es correcto.");
        }

        $this->certificado = $certificate;

        $key = CfdiBase::getPrivateKey($url_key, $clave_privada);

        if(!$key){
            throw new CfdiException("El Certificado de Sello Digital (.key) no es correcto o la contraseña es inválida.");
        }

        $this->key = $key;

        $this->timbrador = new CfdiTimbrador($this->rfc, $this->certificate);
        $this->cancelador = new CfdiCancelador($this->rfc, $this->certificate);
    }

    //Results Functions
    public function xml(){
        $cfdi_id = $this->cfdi->id;
        if(CfdiTimbre::where("cfdi_id", $cfdi_id)->count() == 0){
            return $this->xml->getXML();
        }
        else{
            if(file_exists($this->path."/".strtoupper($this->cfdi->uuid).".xml")){
                return file_get_contents($this->path."/".strtoupper($this->cfdi->uuid).".xml");
            }
            else{
                return $this->xml->getXML();
            }
        }
    }

    public function uuid(){
        return $this->cfdi->uuid();
    }

    //OpenSSL Functions
    public static function sealXML($archivo_key, $archivo_cer, $cadena_original){
        $content = file_get_contents($archivo_key);
        $content =  '-----BEGIN CERTIFICATE-----'.PHP_EOL.chunk_split(base64_encode($content), 64, PHP_EOL).'-----END CERTIFICATE-----'.PHP_EOL;
        $archivo_pem = $archivo_key.'.pem';
        file_put_contents($archivo_pem, $content);
        $private = openssl_pkey_get_private(file_get_contents($archivo_key));
        openssl_sign($cadena_original, $firma, $private);
        $sello = base64_encode($firma);
        return $sello;
    }

    public static function sealing($key_path, $cer_path, $cadena){
        $cmd = "openssl dgst sign [".$key_path."] [".sha1($cadena)."] | openssl enc -base64 -A  [".$cer_path."]";
        if ( $result = shell_exec( $cmd ) ) {
            unset( $cmd );
            return $result;
        }
        return false;
    }

    public static function getPrivateKey ( $key_path, $password ){
        $cmd = "openssl pkcs8 -inform DER -in ".$key_path." -passin pass:'".$password."'";
        if ( $result = shell_exec( $cmd ) ) {
            unset( $cmd );
            return $result;
        }
        return false;
    }

    public static function getCertificate ( $cer_path, $to_string = true ){
        $cmd = 'openssl x509 -inform DER -outform PEM -in '.$cer_path.' -pubkey';
        if ( $result = shell_exec( $cmd ) ) {
            unset( $cmd );
            if ( $to_string ) {
                return $result;
            }
            $split = preg_split( '/\n(-*(BEGIN|END)\sCERTIFICATE-*\n)/', $result );
            unset( $result );
            return preg_replace( '/\n/', '', $split[1] );
        }
        return false;
    }

    public static function getInfoCertificate ( $cer_path, $to_string = true ){
        $cer_path = str_replace("\\","/", $cer_path);

        if(!file_exists($cer_path)){
            $response = [
                "rfc" => "",
                "name" => "",
                "serial" => "",
                "start_date" => "0000-00-00",
                "end_date" => "0000-00-00",
                "type" => "CSD"
            ];

            return $response;
        }

        $cmd = 'openssl x509 -inform DER -in '.$cer_path.' -serial -dates -subject';

        $result = shell_exec( $cmd );

        $result = explode("\n", $result);

        $type = "CSD";
        $name = "";

        foreach($result as $line){
            if(strpos($line, 'subject') === 0){
                $line = str_replace(" ", "", $line);

                if(strpos($line, "OU=") !== false){
                    $type = "CSD";
                }
                else{
                    $type = "FIEL";
                }

                break;
            }
        }

        $serial = ""; $notBefore = ""; $notAfter = ""; $rfc = "";

        for($i = 0; $i < count($result); $i++){
            $line = $result[$i];
 
            if(strpos($line, "serial") !== false && $serial == ""){
                $serial = str_replace("serial=", "", $line);

                $serial_number = "";

                for($k = 1; $k < strlen($serial); $k += 2){
                    $serial_number .= $serial[$k];
                }
            }

            if(strpos($line, "notBefore") !== false){
                $notBefore = str_replace("notBefore=", "", $line);
            }

            if(strpos($line, "notAfter") !== false){
                $notAfter = str_replace("notAfter=", "", $line);
            }

            if(strpos($line, "subject") !== false){
                $line_aux = str_replace(" ", "", $line);

                $line_aux = substr($line_aux, strpos($line_aux, "UniqueIdentifier=") + 17);

                $rfc = trim(substr($line_aux, 0, strpos($line_aux, "/")));

                if(!$rfc){
                    $rfc = trim(substr($line_aux, 0, strpos($line_aux, ",")));
                }
            }

            if(strpos($line, "subject") !== false){
                $line_aux = $line;
                $line_aux = substr($line_aux, strpos($line_aux, "CN")+2);
                $line_aux = substr($line_aux, 0, strpos($line_aux, ","));
                $line_aux = str_replace("=", "", $line_aux);
                $line_aux = trim($line_aux);

                $name = $line_aux;
            }

            if($serial != "" && $notBefore != "" && $notAfter != "" && $rfc != ""){
                break;
            }
        }

        $notBefore = str_replace("    "," ", $notBefore);
        $notBefore = str_replace("   "," ", $notBefore);
        $notBefore = str_replace("  "," ", $notBefore);

        $notBefore = explode(" ", $notBefore);

        $d1 = str_pad($notBefore[1],2,"0", STR_PAD_LEFT);
        $m1 = 1;
        $y1 = str_pad($notBefore[3],4,"0", STR_PAD_LEFT);;

        switch($notBefore[0]){
            case "Jan": $m1 = 1; break;
            case "Feb": $m1 = 2; break;
            case "Mar": $m1 = 3; break;
            case "Apr": $m1 = 4; break;
            case "May": $m1 = 5; break;
            case "Jun": $m1 = 6; break;
            case "Jul": $m1 = 7; break;
            case "Aug": $m1 = 8; break;
            case "Sep": $m1 = 9; break;
            case "Oct": $m1 = 10; break;
            case "Nov": $m1 = 11; break;
            case "Dec": $m1 = 12; break;
        }

        $m1 = str_pad($m1,2,"0", STR_PAD_LEFT);

        $notAfter = str_replace("    "," ", $notAfter);
        $notAfter = str_replace("   "," ", $notAfter);
        $notAfter = str_replace("  "," ", $notAfter);

        $notAfter = explode(" ", $notAfter);

        $d2 = str_pad($notAfter[1],2,"0", STR_PAD_LEFT);;
        $m2 = 1;
        $y2 = str_pad($notAfter[3],4,"0", STR_PAD_LEFT);;

        switch($notAfter[0]){
            case "Jan": $m2 = 1; break;
            case "Feb": $m2 = 2; break;
            case "Mar": $m2 = 3; break;
            case "Apr": $m2 = 4; break;
            case "May": $m2 = 5; break;
            case "Jun": $m2 = 6; break;
            case "Jul": $m2 = 7; break;
            case "Aug": $m2 = 8; break;
            case "Sep": $m2 = 9; break;
            case "Oct": $m2 = 10; break;
            case "Nov": $m2 = 11; break;
            case "Dec": $m2 = 12; break;
        }

        $m2 = str_pad($m2,2,"0", STR_PAD_LEFT);

        $response = [
            "rfc" => $rfc,
            "name" => $name,
            "serial" => $serial_number,
            "start_date" => $y1."-".$m1."-".$d1,
            "end_date" => $y2."-".$m2."-".$d2,
            "type" => $type
        ];

        return $response;
    }

    public static function signData ( $key, $data ){
        $pkeyid = openssl_get_privatekey( $key );
        //$data = "|".substr($data,3);
        //$data = substr($data,0,strlen($data)-4)."||";

        if ( openssl_sign( $data, $cryptedata, $pkeyid, OPENSSL_ALGO_SHA256 ) ) {
            openssl_free_key( $pkeyid );
            return base64_encode( $cryptedata );
        }
    }

    public static function getSerialFromCertificate ( $cer_path ){
        $cmd = 'openssl x509 -inform DER -outform PEM -in '.$cer_path.' -pubkey | '.'openssl x509 -serial -noout';

        if ( $serial = shell_exec( $cmd ) ) {
            unset( $cmd );
            if ( preg_match( "/([0-9]{40})/", $serial, $match ) ) {
                unset( $serial );
                return implode( '', array_map( 'chr', array_map( 'hexdec', str_split( $match[1], 2 ) ) ) );
            }
        }

        return false;
    }

    public static function xmlValidation($xml_path, $xsd_path){
        $xml = new \DOMDocument();

        $xml->load( $xml_path, LIBXML_NOCDATA );

        $validate = $xml->schemaValidate($xsd_path);

        if($validate) return $xml;

        return false;
    }

    public static function getOriginalString($xml_path, $xslt_path){
        $xslt = new XsltProcessor();
        $xsl = new \DOMDocument();
        $xml = new \DOMDocument();

        $xsl->load( $xslt_path, LIBXML_NOCDATA);
        $xml->load( $xml_path, LIBXML_NOCDATA );

        $xslt->importStylesheet( $xsl );

        $original_string = $xslt->transformToXML( $xml );

        $original_string = $xslt->transformToXML( $xml );
        $original_string = str_replace("\n","",$original_string);
        $original_string = str_replace("    ","",$original_string);

        return $original_string;
    }

    //Setters
    public function setProduction($production){
        $this->production = $production;
    }

    public function setPath($path = false){
        if(!$path){
            $path = public_path()."/cfdis";
        }

        $this->path = $path;
    }

    //Validations
    public function validate($cfdi){
        if(!$cfdi->emisor){
            throw new CfdiException("El emisor de la factura (Cfdi) no ha sido definido.");
        }

        if(!$cfdi->receptor){
            throw new CfdiException("El receptor de la factura (Cfdi) no ha sido definido.");
        }

        $conceptos = $cfdi->conceptos()->count();

        if($conceptos == 0){
            throw new CfdiException("La factura (Cfdi) debe contener al menos un concepto a facturar.");
        }
    }
}
