<?php
namespace Codelko\Ciberfactura\Libraries;

use Codelko\Ciberfactura\Libraries\Complementos\Pago;
use Codelko\Ciberfactura\Libraries\Complementos\Terceros;
use Codelko\Ciberfactura\Libraries\Complementos\TimbreFiscalDigital;
use Codelko\Ciberfactura\Models\V40\CfdiConcepto;
use Codelko\Ciberfactura\Models\CfdiConceptoTerceros;
use Codelko\Ciberfactura\Models\Pagos\CfdiPago;
use Codelko\Ciberfactura\Models\Pagos\CfdiPagoRelacion;
use Codelko\Ciberfactura\Models\V40\CfdiTerceros;

class CfdiV40Generator{
    public $xml;
    public $cfdi;
    public $complemento;

    public $xmlns;
    public $schemaLocation;

    public function __construct($cfdi, $decimals = 2){
        $this->cfdi = $cfdi;

        if($cfdi->version == "3.2") return;
        if($cfdi->version == "3.3") return;

        $this->complemento = new CfdiNodo("cfdi:Complemento");

        $this->xmlns['xmlns:tdCFDI'] = "http://www.sat.gob.mx/sitio_internet/cfd/tipoDatos/tdCFDI";
        $this->xmlns['xmlns:catCFDI'] = "http://www.sat.gob.mx/sitio_internet/cfd/catalogos";
        $this->xmlns['xmlns:cfdi'] = "http://www.sat.gob.mx/cfd/4";
        $this->xmlns['xmlns:xsi'] = "http://www.w3.org/2001/XMLSchema-instance";

        $this->schemaLocation[] = 'http://www.sat.gob.mx/cfd/4';
        $this->schemaLocation[] = 'http://www.sat.gob.mx/sitio_internet/cfd/4/cfdv40.xsd';
        $this->schemaLocation[] = 'http://www.sat.gob.mx/sitio_internet/cfd/catalogos';
        $this->schemaLocation[] = 'http://www.sat.gob.mx/sitio_internet/cfd/catalogos/catCFDI.xsd';
        $this->schemaLocation[] = 'http://www.sat.gob.mx/sitio_internet/cfd/tipoDatos/tdCFDI';
        $this->schemaLocation[] = 'http://www.sat.gob.mx/sitio_internet/cfd/tipoDatos/tdCFDI/tdCFDI.xsd';

        //Comprobante
        $comprobante = new CfdiNodo("cfdi:Comprobante");

        //$comprobante->agregarAtributo("xsi:schemaLocation", "http://www.sat.gob.mx/cfd/4 http://www.sat.gob.mx/sitio_internet/cfd/4/cfdv40.xsd http://www.sat.gob.mx/sitio_internet/cfd/catalogos http://www.sat.gob.mx/sitio_internet/cfd/catalogos/catCFDI.xsd http://www.sat.gob.mx/sitio_internet/cfd/tipoDatos/tdCFDI http://www.sat.gob.mx/sitio_internet/cfd/tipoDatos/tdCFDI/tdCFDI.xsd");

        //$comprobante->agregarAtributo("xmlns:tdCFDI", "http://www.sat.gob.mx/sitio_internet/cfd/tipoDatos/tdCFDI");
        //$comprobante->agregarAtributo("xmlns:catCFDI", "http://www.sat.gob.mx/sitio_internet/cfd/catalogos");
        //$comprobante->agregarAtributo("xmlns:cfdi", "http://www.sat.gob.mx/cfd/4");
        //$comprobante->agregarAtributo("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");

        $comprobante->agregarAtributo("Version", $cfdi->version);

        if($cfdi->serie){
            $comprobante->agregarAtributo("Serie", $cfdi->serie);
        }

        if($cfdi->folio){
            $comprobante->agregarAtributo("Folio", $cfdi->folio);
        }


        $comprobante->agregarAtributo("Fecha", str_replace(" ","T", $cfdi->fecha));

        if($cfdi->forma_pago) {
            $comprobante->agregarAtributo("FormaPago", $cfdi->forma_pago);
        }

        $comprobante->agregarAtributo("NoCertificado", $cfdi->no_certificado);
        $comprobante->agregarAtributo("Certificado", $cfdi->certificado);

        if($cfdi->condiciones_de_pago){
            $comprobante->agregarAtributo("CondicionesDePago", $cfdi->condiciones_de_pago);
        }

        $comprobante->agregarAtributo("Moneda", $cfdi->moneda);

        if($cfdi->tipo_cambio){
            $comprobante->agregarAtributo("TipoCambio", $cfdi->tipo_cambio);
        }

        $comprobante->agregarAtributo("TipoDeComprobante", $cfdi->tipo_de_comprobante);

        //Se agrega el campo exportación (4.0)
        $comprobante->agregarAtributo("Exportacion", $cfdi->exportacion ? $cfdi->exportacion : '01');

        if($cfdi->metodo_pago){
            $comprobante->agregarAtributo("MetodoPago", $cfdi->metodo_pago);
        }

        $comprobante->agregarAtributo("LugarExpedicion", $cfdi->lugar_expedicion);

        //Se agrega el campo confirmación (4.0)
        if($cfdi->confirmacion){
            $comprobante->agregarAtributo("Confirmacion", $cfdi->confirmacion);
        }

        //Se agrega el nodo de información global (4.0)
        if($cfdi->periodicidad && $cfdi->periodicidad != "false"){
            $cfdi_informacion_global = new CfdiNodo("cfdi:InformacionGlobal");

            $cfdi_informacion_global->agregarAtributo("Periodicidad", $cfdi->periodicidad);
            $cfdi_informacion_global->agregarAtributo("Meses", $cfdi->meses);
            $cfdi_informacion_global->agregarAtributo("Año", $cfdi->ejercicio);

            $comprobante->agregarNodo($cfdi_informacion_global);
        }

        $cfdi_relaciones = $cfdi->relaciones;

        if($cfdi_relaciones && $cfdi_relaciones->count() > 0){
            $cfdi_relacionados = new CfdiNodo("cfdi:CfdiRelacionados");

            $cfdi_relacionados->agregarAtributo("TipoRelacion", $cfdi_relaciones[0]->tipo_relacion);

            foreach($cfdi_relaciones as $cfdi_relacion){
                $cfdi_relacionado = new CfdiNodo("cfdi:CfdiRelacionado");

                $cfdi_relacionado->agregarAtributo("UUID", $cfdi_relacion->uuid);

                $cfdi_relacionados->agregarNodo($cfdi_relacionado);
            }

            $comprobante->agregarNodo($cfdi_relacionados);
        }

        $this->cfdi = $comprobante;

        $cfdi_emisor = $cfdi->emisor;

        //Emisor
        $emisor = new CfdiNodo("cfdi:Emisor");
        $emisor->agregarAtributo("Rfc", $cfdi_emisor->rfc);
        $emisor->agregarAtributo("Nombre", $cfdi_emisor->nombre);
        $emisor->agregarAtributo("RegimenFiscal", $cfdi_emisor->regimen_fiscal);
        $comprobante->agregarNodo($emisor);

        $cfdi_receptor = $cfdi->receptor;

        //Receptor
        $receptor = new CfdiNodo("cfdi:Receptor");
        $receptor->agregarAtributo("Rfc", $cfdi_receptor->rfc);
        $receptor->agregarAtributo("Nombre", $cfdi_receptor->nombre);

        //Se agrega el domicilio fiscal del receptor (4.0)
        $receptor->agregarAtributo("DomicilioFiscalReceptor", $cfdi_receptor->domicilio_fiscal);

        if($cfdi_receptor->rfc == 'XEXX010101000' && $cfdi_receptor->residencia_fiscal){
            $receptor->agregarAtributo("ResidenciaFiscal", $cfdi_receptor->residencia_fiscal);
        }

        if($cfdi_receptor->num_reg_id_trib){
            $receptor->agregarAtributo("NumRegIdTrib", $cfdi_receptor->num_reg_id_trib);
        }

        //Se agrega el regimen fiscal del receptor (4.0)
        $receptor->agregarAtributo("RegimenFiscalReceptor", $cfdi_receptor->regimen_fiscal);

        $receptor->agregarAtributo("UsoCFDI", $cfdi_receptor->uso_cfdi);
        $comprobante->agregarNodo($receptor);

        $cfdi_conceptos = $cfdi->conceptos;

        //Conceptos
        $conceptos = new CfdiNodo("cfdi:Conceptos");

        $subtotal = 0;
        $total = 0;

        $flag = false;

        if($cfdi_conceptos) foreach($cfdi_conceptos as $cfdi_concepto){
            $concepto = new CfdiNodo("cfdi:Concepto");

            $concepto->agregarAtributo("ClaveProdServ", $cfdi_concepto->clave_prod_serv);

            if($cfdi_concepto->no_identificacion){
                $concepto->agregarAtributo("NoIdentificacion", $cfdi_concepto->no_identificacion);
            }

            if($cfdi->tipo_de_comprobante == "P" || $cfdi->tipo_de_comprobante == "N"){
                $concepto->agregarAtributo("Cantidad", "1");
            }
            else{
                $concepto->agregarAtributo("Cantidad", number_format($cfdi_concepto->cantidad,2,".",""));
            }

            $concepto->agregarAtributo("ClaveUnidad", $cfdi_concepto->clave_unidad);

            if($cfdi_concepto->unidad){
                $concepto->agregarAtributo("Unidad", $cfdi_concepto->unidad);
            }

            $concepto->agregarAtributo("Descripcion", str_replace("<br>"," ", $cfdi_concepto->descripcion));
            
            if($cfdi_concepto->descuento > 0.00 || ($cfdi->tipo_de_comprobante == "N" && $cfdi_concepto->descuento >= 0.00)){
                $concepto->agregarAtributo("Descuento", number_format($cfdi_concepto->descuento,2,".",""));
            }

            //Se agrega el objeto imp en los conceptos (4.0)
            if($cfdi_concepto->impuestos()->count() > 0){
                $concepto->agregarAtributo("ObjetoImp", "02"); //02 - Sí objeto de impuesto.
            }
            else{
                $concepto->agregarAtributo("ObjetoImp", "01"); //01 - No objeto de impuesto.
            }

            //INICIA EL NODO DE TERCEROS
            if(CfdiTerceros::where("cfdi_concepto_id", $cfdi_concepto->id)->count() > 0){
                $cfdi_terceros = CfdiTerceros::where("cfdi_concepto_id", $cfdi_concepto->id)->first();

                $terceros = new CfdiNodo("cfdi:ACuentaTerceros");

                $terceros->agregarAtributo("RfcACuentaTerceros", $cfdi_terceros->rfc);
                $terceros->agregarAtributo("NombreACuentaTerceros", $cfdi_terceros->nombre);
                $terceros->agregarAtributo("RegimenFiscalACuentaTerceros", $cfdi_terceros->regimen_fiscal);
                $terceros->agregarAtributo("DomicilioFiscalACuentaTerceros", $cfdi_terceros->domicilio_fiscal);

                $concepto->agregarNodo($terceros);
            }
            //TERMINA EL NODO DE TERCEROS

            $valor_unitario = 0;
            if($cfdi->tipo_de_comprobante != "P"){
                $valor_unitario = number_format($cfdi_concepto->valor_unitario, $decimals,".","");
            }

            $importe = 0;
            if($cfdi->tipo_de_comprobante != "P"){
                $importe = number_format($cfdi_concepto->importe, $decimals,".","");
            }

            $concepto->agregarAtributo("ValorUnitario", $valor_unitario);
            $concepto->agregarAtributo("Importe", $importe);

            $subtotal += $cfdi_concepto->importe;
            $total += $cfdi_concepto->importe;
            
            $impuestos = new CfdiNodo("cfdi:Impuestos");

            $traslados = new CfdiNodo("cfdi:Traslados");
            $retenciones = new CfdiNodo("cfdi:Retenciones");

            $cfdi_impuestos = $cfdi_concepto->impuestos;

            if($cfdi_impuestos) foreach($cfdi_impuestos as $cfdi_impuesto){
                if(strtolower($cfdi_impuesto->type) == "traslado"){
                    $traslado = new CfdiNodo("cfdi:Traslado");

                    $traslado->agregarAtributo("Base", number_format($cfdi_impuesto->base, $decimals,".",""));
                    $traslado->agregarAtributo("Impuesto", $cfdi_impuesto->impuesto);
                    $traslado->agregarAtributo("TipoFactor", $cfdi_impuesto->tipo_factor);

                    if($cfdi_impuesto->tipo_factor == "Tasa" || $cfdi_impuesto->tipo_factor == "Cuota"){
                        $traslado->agregarAtributo("TasaOCuota", $cfdi_impuesto->tasa_o_cuota);
                        $traslado->agregarAtributo("Importe", number_format($cfdi_impuesto->importe, $decimals,".",""));
                        $total += $cfdi_impuesto->importe;
                    }

                    $traslados->agregarNodo($traslado);
                }

                if(strtolower($cfdi_impuesto->type) == "retencion"){
                    $retencion = new CfdiNodo("cfdi:Retencion");

                    $retencion->agregarAtributo("Base", number_format($cfdi_impuesto->base, $decimals,".",""));
                    $retencion->agregarAtributo("Impuesto", $cfdi_impuesto->impuesto);
                    $retencion->agregarAtributo("TipoFactor", $cfdi_impuesto->tipo_factor);

                    if($cfdi_impuesto->tipo_factor == "Tasa" || $cfdi_impuesto->tipo_factor == "Cuota"){
                        $retencion->agregarAtributo("TasaOCuota", $cfdi_impuesto->tasa_o_cuota);
                        $retencion->agregarAtributo("Importe", number_format($cfdi_impuesto->importe, $decimals,".",""));
                        $total -= $cfdi_impuesto->importe;
                    }

                    $retenciones->agregarNodo($retencion);
                }
            }

            if(count($traslados->nodos) > 0){
                $impuestos->agregarNodo($traslados);
            }

            if(count($retenciones->nodos) > 0){
                $impuestos->agregarNodo($retenciones);
            }

            if(count($impuestos->nodos) > 0){
                $concepto->agregarNodo($impuestos);
            }

            $conceptos->agregarNodo($concepto);
        }

        $comprobante->agregarNodo($conceptos);

        $impuestos = new CfdiNodo("cfdi:Impuestos");

        $codigos_traslado = $cfdi->impuestos()->select("impuesto")->where("type","traslado")->groupBy("impuesto")->get();
        $codigos_retencion = $cfdi->impuestos()->select("impuesto")->where("type","retencion")->groupBy("impuesto")->get();

        if($codigos_retencion->count() > 0){
            $retenciones = new CfdiNodo("cfdi:Retenciones");

            $retenciones_total = 0;

            foreach($codigos_retencion as $codigo){
                $importe = $cfdi->impuestos()->where("type","retencion")->where("impuesto", $codigo->impuesto)->sum("importe");

                $retencion = new CfdiNodo("cfdi:Retencion");
                $retencion->agregarAtributo("Impuesto", $codigo->impuesto);
                $retencion->agregarAtributo("Importe", number_format($importe, $decimals,".",""));

                $retenciones->agregarNodo($retencion);

                $retenciones_total += $importe;
            }

            $impuestos->agregarAtributo("TotalImpuestosRetenidos", number_format($retenciones_total,2,".",""));
            $impuestos->agregarNodo($retenciones);
        }

        if($codigos_traslado->count() > 0){
            $traslados = new CfdiNodo("cfdi:Traslados");

            $traslados_total = 0;

            $cfdi_impuestos = $cfdi->impuestos()->where("type","traslado")->get();

            $tmp = false;
            $bases = false;
            foreach($cfdi_impuestos as $impuesto){
                $tax_index = $impuesto->impuesto."|".$impuesto->tipo_factor."|".$impuesto->tasa_o_cuota;

                if(isset($tmp[$tax_index])){
                    $tmp[$tax_index] += $impuesto->importe;
                    $bases[$tax_index] += $impuesto->base;
                }
                else{
                    $tmp[$tax_index] = $impuesto->importe;
                    $bases[$tax_index] = $impuesto->base;
                }
            }

            if($tmp){
                foreach($tmp as $key => $impuesto){
                    $base = $bases[$key];
                    $key = explode("|", $key);
    
                    $traslado = new CfdiNodo("cfdi:Traslado");
    
                    $traslado->agregarAtributo("Base", number_format($base, $decimals,".",""));
    
                    $traslado->agregarAtributo("Impuesto", $key[0]);
                    $traslado->agregarAtributo("TipoFactor", $key[1]);
    
                    if($key[1] == "Tasa" || $key[1] == "Cuota"){
                        $traslado->agregarAtributo("TasaOCuota", $key[2]);
                        $traslado->agregarAtributo("Importe", number_format($impuesto, $decimals,".",""));
                    }
    
                    $traslados->agregarNodo($traslado);
    
                    $traslados_total += $impuesto;
                }
            }

            $impuestos->agregarAtributo("TotalImpuestosTrasladados", number_format($traslados_total,2,".",""));
            $impuestos->agregarNodo($traslados);
        }

        if($codigos_retencion->count() > 0 || $codigos_traslado->count() > 0) {
            if ($cfdi->tipo_de_comprobante != "P" && $cfdi->tipo_de_comprobante != "T" && $cfdi->tipo_de_comprobante != "N") {
                $comprobante->agregarNodo($impuestos);
            }
        }

        $cfdi_complementos = $cfdi->complementos;

        if($cfdi_complementos) foreach($cfdi_complementos as $cfdi_complemento){
            if($cfdi_complemento->name == "pago10"){
                $pagos = CfdiPago::where("cfdi_id", $cfdi->id)->get();

                if($pagos) foreach($pagos as $pago){
                    $pago->fecha_pago = str_replace(" ","T", $pago->fecha_pago);

                    $complemento = new Pago($pago);

                    $relaciones = CfdiPagoRelacion::where("cfdi_id", $cfdi->id)->where("cfdi_pago_id", $pago->id)->get();

                    if($relaciones) foreach($relaciones as $relacion){
                        $complemento->addRelacion($relacion);
                    }
                }
            }

            if(isset($complemento)){
                $this->complementar($complemento);
            }
        }

        if($cfdi->sello){
            $this->sellar($cfdi->sello);
        }

        if($cfdi->timbre){
            $timbre = new TimbreFiscalDigital($cfdi->timbre->uuid, $cfdi->timbre->fecha_timbrado, $cfdi->timbre->sello_cfd, $cfdi->timbre->rfc_pac, $cfdi->timbre->no_certificado_sat,$cfdi->timbre->sello_sat);
            $this->timbrar($timbre);
        }

        $comprobante->agregarAtributo("SubTotal", $cfdi->sub_total > 0 ? number_format($cfdi->sub_total > 0 ? $cfdi->sub_total : 0,2,".","") : 0);
        
        if($cfdi->descuento > 0.00 || ($cfdi->tipo_de_comprobante == "N" && $cfdi->descuento >= 0.00)){
            $comprobante->agregarAtributo("Descuento", $cfdi->descuento > 0 ? number_format($cfdi->descuento,2,".","") : 0);
        }

        $comprobante->agregarAtributo("Total", $cfdi->total > 0 ? number_format($cfdi->total > 0 ? $cfdi->total : 0,2,".","") : 0);

        $this->cfdi = $comprobante;
    }

    public function complementar(CfdiComplemento $complemento){
        $nss = $complemento->getNS();

        if($nss) foreach($nss as $ns => $url){
            $this->xmlns[$ns] = $url;
        }

        $locations = $complemento->getSchemaLocation();

        if($locations) foreach($locations as $location){
            $this->schemaLocation[] = $location;
        }

        $this->complemento->agregarNodo($complemento->getNodo());
    }

    public function addendar(){
        //Addenda
        $addenda = new CfdiNodo("cfdi:Addenda");
        $this->cfdi->agregarNodo($addenda);
    }

    public function timbrar(TimbreFiscalDigital $timbre){
        $this->complemento->agregarNodo($timbre->getNodo());
    }

    public function sellar($sello){
        $this->cfdi->agregarAtributo("Sello", $sello);
    }

    public function getXML($format = false){
        $generator = clone $this->cfdi;

        //Agrega Namespaces
        $generator->agregarAtributo("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
        $generator->agregarAtributo("xsi:schemaLocation", implode(" ", $this->schemaLocation));

        if($this->xmlns) foreach($this->xmlns as $ns => $xmlns){
            $generator->agregarAtributo($ns, $xmlns);
        }

        $tfd = false;

        if($this->complemento->tieneAtributos() || $this->complemento->tieneNodos()){
            $generator->agregarNodo($this->complemento);
        }

        $this->xml = new \DOMDocument('1.0' , 'UTF-8');
        $this->xml->appendChild($this->xmlizar($generator));
        $this->xml->preserveWhiteSpace = false;
        $this->xml->formatOutput = $format;
        $xml = $this->xml->saveXML();

        return $xml;
    }

    public function saveFile($file, $format = false){
        $generator = clone $this->cfdi;

        //Agrega Namespaces
        $generator->agregarAtributo("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
        $generator->agregarAtributo("xsi:schemaLocation", implode(" ", $this->schemaLocation));

        if($this->xmlns) foreach($this->xmlns as $ns => $xmlns){
            $generator->agregarAtributo($ns, $xmlns);
        }

        $generator->agregarNodo($this->complemento);

        $this->xml = new \DOMDocument('1.0' , 'UTF-8');
        $this->xml->appendChild($this->xmlizar($generator));
        $this->xml->preserveWhiteSpace = false;
        $this->xml->formatOutput = $format;
        $this->xml->save($file);
    }

    public function completeFile($file, $format = false){
        $generator = clone $this->cfdi;

        //Agrega Namespaces
        $generator->agregarAtributo("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
        $generator->agregarAtributo("xsi:schemaLocation", implode(" ", $this->schemaLocation));

        if($this->xmlns) foreach($this->xmlns as $ns => $xmlns){
            $generator->agregarAtributo($ns, $xmlns);
        }

        $generator->agregarNodo($this->complemento);

        $this->xml = new \DOMDocument('1.0' , 'UTF-8');
        $this->xml->appendChild($this->xmlizar($generator));
        $this->xml->preserveWhiteSpace = false;
        $this->xml->formatOutput = $format;
        $this->xml->save($file);
    }

    public function saveXML($file, $format = false){
        $this->saveFile($file, $format);
    }

    public function xmlizar($nodo){
        if($nodo->value){
            $xml = $this->xml->createElement($nodo->nombre, $nodo->value);
        }
        else{
            $xml = $this->xml->createElement($nodo->nombre);
        }

        if($nodo->atributos) foreach($nodo->atributos as $nombre => $valor){
            $tmp = $this->xml->createAttribute($nombre);
            $tmp->value = $valor;
            $xml->appendChild($tmp);
        }

        if($nodo->nodos) foreach($nodo->nodos as $n){
            $tmp = $this->xmlizar($n);
            $xml->appendChild($tmp);
        }

        return $xml;
    }
}