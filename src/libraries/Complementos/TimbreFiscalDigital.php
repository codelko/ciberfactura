<?php

namespace Codelko\Ciberfactura\Libraries\Complementos;

use Codelko\Ciberfactura\Libraries\CfdiComplemento;
use Codelko\Ciberfactura\Libraries\CfdiNodo;
use Codelko\Ciberfactura\Libraries\CfdiStamp;
use Genkgo\Xsl\XsltProcessor;

class TimbreFiscalDigital extends CfdiComplemento {
    public $version = "1.1";
    public $name = "tfd";
    public $xmlns = ['xmlns:tfd' => "http://www.sat.gob.mx/TimbreFiscalDigital"];
    public $schemaLocation = ['http://www.sat.gob.mx/TimbreFiscalDigital', 'http://www.sat.gob.mx/sitio_internet/cfd/TimbreFiscalDigital/TimbreFiscalDigitalv11.xsd'];

    public $xslt = "cadenaoriginal_TFD_1_0.xslt";

    public $uuid;
    public $fecha_timbrado;
    public $sello_cfd;
    public $rfc_pac;
    public $no_certificado;
    public $sello_sat;

    public function __construct($uuid, $fecha_timbrado, $sello_cfd, $rfc_pac, $no_certificado, $sello_sat){
        $this->uuid = $uuid;
        $this->fecha_timbrado = $fecha_timbrado;
        $this->sello_cfd = $sello_cfd;
        $this->rfc_pac = $rfc_pac;
        $this->no_certificado = $no_certificado;
        $this->sello_sat = $sello_sat;
    }

    public function getNS(){
        return $this->xmlns;
    }

    public function getSchemaLocation(){
        return $this->schemaLocation;
    }

    public function getOriginalString(){
        $xslt = new XsltProcessor();
        $xsl = new \DOMDocument();
        $xml = new \DOMDocument();

        $xslt_file = __DIR__.'/../../resources/xslt/cadenaoriginal_TFD_1_1.xslt';

        if($this->version == "1.0"){
            $xslt_file = __DIR__.'/../../resources/xslt/cadenaoriginal_TFD_1_0.xslt';
        }

        $xml_temp = __DIR__.'/../../resources/'.uniqid().'.xml';

        file_put_contents($xml_temp, $this->getXML());

        $xsl->load( $xslt_file, LIBXML_NOCDATA);
        $xml->load( $xml_temp , LIBXML_NOCDATA );

        $xslt->importStylesheet( $xsl );

        $original_string = $xslt->transformToXML( $xml );
        $original_string = str_replace("\n","",$original_string);
        $original_string = str_replace("    ","",$original_string);

        @unlink($xml_temp);

        return $original_string;
    }

    public function getNodo(){
        $nodo = new CfdiNodo("tfd:TimbreFiscalDigital");

        $nodo->agregarAtributo("xsi:schemaLocation", implode(" ", $this->getSchemaLocation()));

        $nodo->agregarAtributo("Version", $this->version);
        $nodo->agregarAtributo("UUID", strtolower($this->uuid));
        $nodo->agregarAtributo("FechaTimbrado", str_replace(" ","T", $this->fecha_timbrado));
        $nodo->agregarAtributo("RfcProvCertif", $this->rfc_pac);
        $nodo->agregarAtributo("SelloCFD", $this->sello_cfd);
        $nodo->agregarAtributo("NoCertificadoSAT", $this->no_certificado);
        $nodo->agregarAtributo("SelloSAT", $this->sello_sat);


        $nodo->agregarAtributo("xmlns:tfd", "http://www.sat.gob.mx/TimbreFiscalDigital");
        $nodo->agregarAtributo("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");

        return $nodo;
    }
}