<?php

namespace Codelko\Ciberfactura\Libraries\Complementos;

use Codelko\Ciberfactura\Libraries\CfdiComplemento;
use Codelko\Ciberfactura\Libraries\CfdiNodo;
use Codelko\Ciberfactura\Models\CfdiFactura;
use Codelko\Ciberfactura\Models\CfdiImpuesto;
use Codelko\Ciberfactura\Models\Pagos\V40\CfdiPago;
use Codelko\Ciberfactura\Models\Pagos\V40\CfdiPagoImpuesto;
use Codelko\Ciberfactura\Models\Pagos\V40\CfdiPagoRelacion;
use Codelko\Ciberfactura\Models\V40\CfdiFactura as V40CfdiFactura;
use Codelko\Ciberfactura\Models\V40\CfdiImpuesto as V40CfdiImpuesto;

class Pago20 extends CfdiComplemento {
    public $version = "2.0";

    public $name = "pago20";
    public $xmlns = ['xmlns:pago20' => 'http://www.sat.gob.mx/Pagos20'];
    public $schemaLocation = ['http://www.sat.gob.mx/Pagos20', 'http://www.sat.gob.mx/sitio_internet/cfd/Pagos/Pagos20.xsd'];

    public $pagos;
    public $monto = 0;

    public $relaciones = [];
    public $impuestos = [];

    public function __construct(CfdiPago $pago){
        $this->pagos[] = $pago;
        $this->monto = $pago->monto;
    }

    public function getNS(){
        return $this->xmlns;
    }

    public function getSchemaLocation(){
        return $this->schemaLocation;
    }

    public function addPago(CfdiPago $pago){
        $this->pagos[] = $pago;
        $this->monto += $pago->monto;
    }

    public function addRelacion(CfdiPagoRelacion $relacion){
        $this->relaciones[] = $relacion;

        $cfdi = CfdiFactura::where("uuid", $relacion->id_documento)->first();

        if(!$cfdi){
            $cfdi = V40CfdiFactura::where("uuid", $relacion->id_documento)->first();
        }

        if($cfdi->version == "4.0"){
            $impuestos = V40CfdiImpuesto::where("cfdi_id", $cfdi->id)->get();

            $factor_pago = $relacion->importe_pagado / $cfdi->total;

            $taxes = [];

            if($impuestos){
                $base = 0;
                $importe = 0;
                foreach($impuestos as $impuesto){
                    if(!isset($taxes[$impuesto->type."|".$impuesto->impuesto."|".$impuesto->tipo_factor."|".$impuesto->tasa_o_cuota])){
                        $taxes[$impuesto->type."|".$impuesto->impuesto."|".$impuesto->tipo_factor."|".$impuesto->tasa_o_cuota]["base"] = $impuesto->base;
                        $taxes[$impuesto->type."|".$impuesto->impuesto."|".$impuesto->tipo_factor."|".$impuesto->tasa_o_cuota]["importe"] = $impuesto->importe;
                    }
                    else{
                        $taxes[$impuesto->type."|".$impuesto->impuesto."|".$impuesto->tipo_factor."|".$impuesto->tasa_o_cuota]["base"] += $impuesto->base; 
                        $taxes[$impuesto->type."|".$impuesto->impuesto."|".$impuesto->tipo_factor."|".$impuesto->tasa_o_cuota]["importe"] += $impuesto->importe;
                    }
                }

                foreach($taxes as $key => $tax){
                    $keys = explode("|", $key);

                    $pago_impuesto = CfdiPagoImpuesto::create([
                        'cfdi_id' => $relacion->cfdi_id,
                        'cfdi_pago_id' => $relacion->cfdi_pago_id,
                        'cfdi_relacion_id' => $relacion->id,
                        'type' => $keys[0],
                        'base' => number_format($tax["base"] * $factor_pago,2,".",""),
                        'impuesto' => $keys[1],
                        'tipo_factor' => $keys[2],
                        'tasa_o_cuota' => $keys[3],
                        'importe' => number_format($tax["importe"] * $factor_pago,2,".",""),
                    ]);
    
                    $this->addImpuesto($pago_impuesto);
                }
            }
        }
        else{
            $impuestos = CfdiImpuesto::where("cfdi_id", $cfdi->id)->get();

            $factor_pago = $relacion->importe_pagado / $cfdi->total;

            if($impuestos) foreach($impuestos as $impuesto){
                $pago_impuesto = CfdiPagoImpuesto::create([
                    'cfdi_id' => $relacion->cfdi_id,
                    'cfdi_pago_id' => $relacion->cfdi_pago_id,
                    'cfdi_relacion_id' => $relacion->id,
                    'type' => $impuesto->type,
                    'base' => number_format($impuesto->base * $factor_pago,2,".",""),
                    'impuesto' => $impuesto->impuesto,
                    'tipo_factor' => $impuesto->tipo_factor,
                    'tasa_o_cuota' => $impuesto->tasa_o_cuota,
                    'importe' => number_format($impuesto->importe * $factor_pago,2,".",""),
                ]);

                $this->addImpuesto($pago_impuesto);
            }
        }
    }

    public function addImpuesto(CfdiPagoImpuesto $impuesto){
        $this->impuestos[] = $impuesto;
    }

    public function getTotales(){
        $totales = [
            "monto_total_pagos" => 0,
            "total_retenciones_isr" => 0,
            "total_retenciones_iva" => 0,
            "total_retenciones_ieps" => 0,
            "total_traslados_base_iva_16" => 0,
            "total_traslados_impuesto_iva_16" => 0,
            "total_traslados_base_iva_8" => 0,
            "total_traslados_impuesto_iva_8" => 0,
            "total_traslados_base_iva_0" => 0,
            "total_traslados_impuesto_iva_0" => 0,
            "total_traslados_base_iva_exento" => 0
        ];

        $tipo_cambio = $this->pagos[0]->tipo_cambio > 0 ? $this->pagos[0]->tipo_cambio : 1.0000;

        if($this->relaciones){
            foreach($this->relaciones as $relacion){
                $totales["monto_total_pagos"] += $relacion->importe_pagado / $relacion->tipo_cambio;
            }
        }

        if(count($this->impuestos) > 0){
            foreach($this->impuestos as $impuesto){
                if(strtoupper($impuesto->type) == "TRASLADO"){
                    if(strtoupper($impuesto->tipo_factor) == "EXENTO"){
                        $totales["total_traslados_base_iva_exento"] += $impuesto->base / $tipo_cambio;
                    }
                    else{
                        if($impuesto->impuesto == "002"){
                            if($impuesto->tasa_o_cuota == 0.16){
                                $totales["total_traslados_base_iva_16"] += $impuesto->base;
                                $totales["total_traslados_impuesto_iva_16"] += $impuesto->base * $impuesto->tasa_o_cuota;
                            }
                            elseif($impuesto->tasa_o_cuota == 0.08){
                                $totales["total_traslados_base_iva_8"] += $impuesto->base / $tipo_cambio;
                                $totales["total_traslados_impuesto_iva_8"] += $impuesto->base * $impuesto->tasa_o_cuota / $tipo_cambio;
                            }elseif($impuesto->tasa_o_cuota == 0.0){
                                $totales["total_traslados_base_iva_0"] += $impuesto->base / $tipo_cambio;
                                $totales["total_traslados_impuesto_iva_0"] += $impuesto->base * $impuesto->tasa_o_cuota / $tipo_cambio;
                            }
                        }
                    }
                }
                else{
                    if($impuesto->impuesto == "001"){
                        $totales["total_retenciones_isr"] += $impuesto->importe / $tipo_cambio;
                    }

                    if($impuesto->impuesto == "002"){
                        $totales["total_retenciones_iva"] += $impuesto->importe / $tipo_cambio;
                    }

                    if($impuesto->impuesto == "003"){
                        $totales["total_retenciones_ieps"] += $impuesto->importe / $tipo_cambio;
                    }
                }
            }
        }

        return $totales;
    }

    public function getNodo(){
        $nodo = new CfdiNodo("pago20:Pagos");

        $nodo->agregarAtributo("Version", $this->version);

        //Inicia Nodo de Totales
        $totales = $this->getTotales();

        $nodo_totales = new CfdiNodo("pago20:Totales");

        if($totales["total_retenciones_iva"] > 0){
            $nodo_totales->agregarAtributo("TotalRetencionesIVA", number_format($totales["total_retenciones_iva"],2,".",""));
        }

        if($totales["total_retenciones_isr"] > 0){
            $nodo_totales->agregarAtributo("TotalRetencionesISR", number_format($totales["total_retenciones_isr"],2,".",""));
        }

        if($totales["total_retenciones_ieps"] > 0){
            $nodo_totales->agregarAtributo("TotalRetencionesIEPS", number_format($totales["total_retenciones_ieps"],2,".",""));
        }

        if($totales["total_traslados_base_iva_16"] > 0){
            $nodo_totales->agregarAtributo("TotalTrasladosBaseIVA16", number_format($totales["total_traslados_base_iva_16"],2,".",""));
            $nodo_totales->agregarAtributo("TotalTrasladosImpuestoIVA16", number_format($totales["total_traslados_impuesto_iva_16"],2,".",""));
        }

        if($totales["total_traslados_base_iva_8"] > 0){
            $nodo_totales->agregarAtributo("TotalTrasladosBaseIVA8", number_format($totales["total_traslados_base_iva_8"],2,".",""));
            $nodo_totales->agregarAtributo("TotalTrasladosImpuestoIVA8", number_format($totales["total_traslados_impuesto_iva_8"],2,".",""));
        }
        
        if($totales["total_traslados_base_iva_0"] > 0){
            $nodo_totales->agregarAtributo("TotalTrasladosBaseIVA0", number_format($totales["total_traslados_base_iva_0"],2,".",""));
            $nodo_totales->agregarAtributo("TotalTrasladosImpuestoIVA0", number_format($totales["total_traslados_impuesto_iva_0"],2,".",""));
        }

        if($totales["total_traslados_base_iva_exento"] > 0){
            $nodo_totales->agregarAtributo("TotalTrasladosBaseIVAExento", number_format($totales["total_traslados_base_iva_exento"],2,".",""));
        }

        $nodo_totales->agregarAtributo("MontoTotalPagos", number_format($totales["monto_total_pagos"],2,".",""));
        //Termina Nodo de Totales

        $nodo->agregarNodo($nodo_totales);

        if($this->pagos && count($this->pagos) > 0){
            foreach($this->pagos as $pago){
                $nodo_pago = new CfdiNodo("pago20:Pago");

                $nodo_pago->agregarAtributo("FechaPago", $pago->fecha_pago);
                $nodo_pago->agregarAtributo("FormaDePagoP", $pago->forma_pago);
                $nodo_pago->agregarAtributo("MonedaP", $pago->moneda);

                if($pago->moneda != 'MXN'){
                    $nodo_pago->agregarAtributo("TipoCambioP", $pago->tipo_cambio);
                }
                else{
                    $nodo_pago->agregarAtributo("TipoCambioP", "1");
                }

                $nodo_pago->agregarAtributo("Monto", number_format($pago->monto,2,".",""));

                if($pago->num_operacion){
                    $nodo_pago->agregarAtributo("NumOperacion", $pago->num_operacion);
                }

                if($pago->rfc_emisor_cta_ord){
                    $nodo_pago->agregarAtributo("RfcEmisorCtaOrd", $pago->rfc_emisor_cta_ord);
                }

                if($pago->nom_banco_ord_ext){
                    $nodo_pago->agregarAtributo("NomBancoOrdExt", $pago->nom_banco_ord_ext);
                }

                if($pago->cta_ordenante){
                    $nodo_pago->agregarAtributo("CtaOrdenante", $pago->cta_ordenante);
                }

                if($pago->rfc_emisor_cta_ben){
                    $nodo_pago->agregarAtributo("RfcEmisorCtaBen", $pago->rfc_emisor_cta_ben);
                }

                if($pago->cta_beneficiario){
                    $nodo_pago->agregarAtributo("CtaBeneficiario", $pago->cta_beneficiario);
                }

                if($this->relaciones){
                    foreach($this->relaciones as $relacion){
                        $rel_nodo = new CfdiNodo("pago20:DoctoRelacionado");

                        if($relacion->id_documento){
                            $rel_nodo->agregarAtributo("IdDocumento", $relacion->id_documento);
                        }

                        if($relacion->serie){
                            $rel_nodo->agregarAtributo("Serie", $relacion->serie);
                        }

                        if($relacion->folio){
                            $rel_nodo->agregarAtributo("Folio", $relacion->folio);
                        }

                        if($relacion->moneda){
                            $rel_nodo->agregarAtributo("MonedaDR", $relacion->moneda);
                        }

                        if($pago->moneda != $relacion->moneda){
                            if($pago->moneda != "MXN"){
                                $rel_nodo->agregarAtributo("EquivalenciaDR", number_format($pago->tipo_cambio,4,".",""));
                            }
                            else{
                                $rel_nodo->agregarAtributo("EquivalenciaDR", number_format($relacion->tipo_cambio,4,".",""));
                            }
                        }
                        else{
                            $rel_nodo->agregarAtributo("EquivalenciaDR", "1");
                        }

                        if($relacion->num_parcialidad){
                            $rel_nodo->agregarAtributo("NumParcialidad", $relacion->num_parcialidad);
                        }

                        if($pago->moneda != $relacion->moneda){
                            if($pago->moneda != "MXN"){
                                $rel_nodo->agregarAtributo("ImpSaldoAnt", number_format($relacion->importe_saldo_anterior,2,".",""));
                                $rel_nodo->agregarAtributo("ImpPagado", number_format($relacion->importe_pagado,2,".",""));
                                $rel_nodo->agregarAtributo("ImpSaldoInsoluto", number_format($relacion->importe_saldo_insoluto,2,".",""));
                            }
                            else{
                                $rel_nodo->agregarAtributo("ImpSaldoAnt", number_format($relacion->importe_saldo_anterior * $relacion->tipo_cambio,2,".",""));
                                $rel_nodo->agregarAtributo("ImpPagado", number_format($relacion->importe_pagado * $relacion->tipo_cambio,2,".",""));
                                $rel_nodo->agregarAtributo("ImpSaldoInsoluto", number_format($relacion->importe_saldo_insoluto * $relacion->tipo_cambio,2,".",""));
                            }
                        }
                        else{
                            $rel_nodo->agregarAtributo("ImpSaldoAnt", number_format($relacion->importe_saldo_anterior,2,".",""));
                            $rel_nodo->agregarAtributo("ImpPagado", number_format($relacion->importe_pagado,2,".",""));
                            $rel_nodo->agregarAtributo("ImpSaldoInsoluto", number_format($relacion->importe_saldo_insoluto,2,".",""));
                        }

                        if(CfdiPagoImpuesto::where("cfdi_relacion_id", $relacion->id)->count() > 0){
                            $rel_nodo->agregarAtributo("ObjetoImpDR", "02"); //Tiene impuestos

                            //Impuestos
                            $impuestos_pago_nodo = new CfdiNodo("pago20:ImpuestosDR");

                            $grupo_retenciones = [];
                            if(CfdiPagoImpuesto::where("cfdi_relacion_id", $relacion->id)->where("type", "RETENCION")->count() > 0){
                                $retenciones = CfdiPagoImpuesto::where("cfdi_relacion_id", $relacion->id)->where("type", "RETENCION")->get();

                                if($retenciones) foreach($retenciones as $retencion){
                                    if(!isset($grupo_retenciones[$retencion->impuesto."|".$retencion->tipo_factor."|".$retencion->tasa_o_cuota])){
                                        $grupo_retenciones[$retencion->impuesto."|".$retencion->tipo_factor."|".$retencion->tasa_o_cuota]["base"] = $retencion->base / $pago->tipo_cambio;
                                        $grupo_retenciones[$retencion->impuesto."|".$retencion->tipo_factor."|".$retencion->tasa_o_cuota]["impuesto"] = $retencion->impuesto;
                                        $grupo_retenciones[$retencion->impuesto."|".$retencion->tipo_factor."|".$retencion->tasa_o_cuota]["tipo_factor"] = $retencion->tipo_factor;
                                        $grupo_retenciones[$retencion->impuesto."|".$retencion->tipo_factor."|".$retencion->tasa_o_cuota]["tasa_o_cuota"] = $retencion->tasa_o_cuota;
                                        $grupo_retenciones[$retencion->impuesto."|".$retencion->tipo_factor."|".$retencion->tasa_o_cuota]["importe"] = $retencion->base * $retencion->tasa_o_cuota / $pago->tipo_cambio;
                                    }
                                    else{
                                        $grupo_retenciones[$retencion->impuesto."|".$retencion->tipo_factor."|".$retencion->tasa_o_cuota]["base"] += $retencion->base / $pago->tipo_cambio;
                                        $grupo_retenciones[$retencion->impuesto."|".$retencion->tipo_factor."|".$retencion->tasa_o_cuota]["importe"] += $retencion->base * $retencion->tasa_o_cuota / $pago->tipo_cambio;
                                    }
                                }
                            }

                            $grupo_traslados = [];
                            if(CfdiPagoImpuesto::where("cfdi_relacion_id", $relacion->id)->where("type", "TRASLADO")->count() > 0){
                                $traslados = CfdiPagoImpuesto::where("cfdi_relacion_id", $relacion->id)->where("type", "TRASLADO")->get();

                                if($traslados) foreach($traslados as $traslado){
                                    if(strtoupper($traslado->tipo_factor) == "EXENTO"){
                                        if(!isset($grupo_traslados[$traslado->impuesto."|".$traslado->tipo_factor])){
                                            $grupo_traslados[$traslado->impuesto."|".$traslado->tipo_factor]["base"] = $traslado->base / $pago->tipo_cambio;
                                            $grupo_traslados[$traslado->impuesto."|".$traslado->tipo_factor]["impuesto"] = $traslado->impuesto;
                                            $grupo_traslados[$traslado->impuesto."|".$traslado->tipo_factor]["tipo_factor"] = $traslado->tipo_factor;
                                            $grupo_traslados[$traslado->impuesto."|".$traslado->tipo_factor]["tasa_o_cuota"] = $traslado->tasa_o_cuota;
                                            $grupo_traslados[$traslado->impuesto."|".$traslado->tipo_factor]["importe"] = $traslado->base * $traslado->tasa_o_cuota / $pago->tipo_cambio;
                                        }
                                        else{
                                            $grupo_traslados[$traslado->impuesto."|".$traslado->tipo_factor]["base"] += $traslado->base / $pago->tipo_cambio;
                                            $grupo_traslados[$traslado->impuesto."|".$traslado->tipo_factor]["importe"] += $traslado->base * $traslado->tasa_o_cuota / $pago->tipo_cambio;
                                        }
                                    }
                                    else{
                                        if(!isset($grupo_traslados[$traslado->impuesto."|".$traslado->tipo_factor."|".$traslado->tasa_o_cuota])){
                                            $grupo_traslados[$traslado->impuesto."|".$traslado->tipo_factor."|".$traslado->tasa_o_cuota]["base"] = $traslado->base / $pago->tipo_cambio;
                                            $grupo_traslados[$traslado->impuesto."|".$traslado->tipo_factor."|".$traslado->tasa_o_cuota]["impuesto"] = $traslado->impuesto;
                                            $grupo_traslados[$traslado->impuesto."|".$traslado->tipo_factor."|".$traslado->tasa_o_cuota]["tipo_factor"] = $traslado->tipo_factor;
                                            $grupo_traslados[$traslado->impuesto."|".$traslado->tipo_factor."|".$traslado->tasa_o_cuota]["tasa_o_cuota"] = $traslado->tasa_o_cuota;
                                            $grupo_traslados[$traslado->impuesto."|".$traslado->tipo_factor."|".$traslado->tasa_o_cuota]["importe"] = $traslado->base * $traslado->tasa_o_cuota / $pago->tipo_cambio;
                                        }
                                        else{
                                            $grupo_traslados[$traslado->impuesto."|".$traslado->tipo_factor."|".$traslado->tasa_o_cuota]["base"] += $traslado->base / $pago->tipo_cambio;
                                            $grupo_traslados[$traslado->impuesto."|".$traslado->tipo_factor."|".$traslado->tasa_o_cuota]["importe"] += $traslado->base * $traslado->tasa_o_cuota / $pago->tipo_cambio;
                                        }
                                    }
                                }

                                if(count($grupo_retenciones) > 0){
                                    $retenciones_pago_nodo = new CfdiNodo("pago20:RetencionesDR");
            
                                    if($grupo_retenciones) foreach($grupo_retenciones as $grupo_retencion){
                                        $retencion_pago_nodo = new CfdiNodo("pago20:RetencionDR");

                                        $retencion_pago_nodo->agregarAtributo("BaseDR", number_format($grupo_retencion["base"],4,".",""));
                                        $retencion_pago_nodo->agregarAtributo("ImpuestoDR", $grupo_retencion["impuesto"]);
                                        $retencion_pago_nodo->agregarAtributo("TipoFactorDR", $grupo_retencion["tipo_factor"]);
                                        $retencion_pago_nodo->agregarAtributo("TasaOCuotaDR", $grupo_retencion["tasa_o_cuota"]);
                                        $retencion_pago_nodo->agregarAtributo("ImporteDR", number_format($grupo_retencion["importe"],4,".",""));
            
                                        $retenciones_pago_nodo->agregarNodo($retencion_pago_nodo);
                                    }
            
                                    $impuestos_pago_nodo->agregarNodo($retenciones_pago_nodo);
                                }

                                if(count($grupo_traslados) > 0){
                                    $traslados_pago_nodo = new CfdiNodo("pago20:TrasladosDR");

                                    if($grupo_traslados) foreach($grupo_traslados as $grupo_traslado){
                                        $traslado_pago_nodo = new CfdiNodo("pago20:TrasladoDR");

                                        $traslado_pago_nodo->agregarAtributo("BaseDR", number_format($grupo_traslado["base"],4,".",""));
                                        $traslado_pago_nodo->agregarAtributo("ImpuestoDR", $grupo_traslado["impuesto"]);
                                        $traslado_pago_nodo->agregarAtributo("TipoFactorDR", $grupo_traslado["tipo_factor"]);
    
                                        if(strtoupper($traslado->tipo_factor) != "EXENTO"){
                                            $traslado_pago_nodo->agregarAtributo("TasaOCuotaDR", $grupo_traslado["tasa_o_cuota"]);
                                            $traslado_pago_nodo->agregarAtributo("ImporteDR", number_format($grupo_traslado["importe"],4,".",""));
                                        }

                                        $traslados_pago_nodo->agregarNodo($traslado_pago_nodo);
                                    }

                                    $impuestos_pago_nodo->agregarNodo($traslados_pago_nodo);
                                }
                            }

                            $rel_nodo->agregarNodo($impuestos_pago_nodo);
                        }
                        else{
                            $rel_nodo->agregarAtributo("ObjetoImpDR", "01"); //No tiene impuestos
                        }

                        $nodo_pago->agregarNodo($rel_nodo);
                    }
                }

                if(count($this->impuestos) > 0){
                    $impuestos = new CfdiNodo("pago20:ImpuestosP");

                    $grupo_traslados = [];
                    $grupo_retenciones = [];

                    foreach($this->impuestos as $impuesto){
                        if($impuesto->type == "TRASLADO"){
                            if(strtoupper($impuesto->tipo_factor) == "EXENTO"){
                                if(!isset($grupo_traslados[$impuesto->impuesto."|".$impuesto->tipo_factor])){
                                    $grupo_traslados[$impuesto->impuesto."|".$impuesto->tipo_factor]["base"] = $impuesto->base / $pago->tipo_cambio;
                                    $grupo_traslados[$impuesto->impuesto."|".$impuesto->tipo_factor]["impuesto"] = $impuesto->impuesto;
                                    $grupo_traslados[$impuesto->impuesto."|".$impuesto->tipo_factor]["tipo_factor"] = $impuesto->tipo_factor;
                                    $grupo_traslados[$impuesto->impuesto."|".$impuesto->tipo_factor]["tasa_o_cuota"] = $impuesto->tasa_o_cuota;
                                    $grupo_traslados[$impuesto->impuesto."|".$impuesto->tipo_factor]["importe"] = $impuesto->base * $impuesto->tasa_o_cuota / $pago->tipo_cambio;
                                }
                                else{
                                    $grupo_traslados[$impuesto->impuesto."|".$impuesto->tipo_factor]["base"] += $impuesto->base / $pago->tipo_cambio;
                                    $grupo_traslados[$impuesto->impuesto."|".$impuesto->tipo_factor]["importe"] += $impuesto->base * $impuesto->tasa_o_cuota / $pago->tipo_cambio;
                                }
                            }
                            else{
                                if(!isset($grupo_traslados[$impuesto->impuesto."|".$impuesto->tipo_factor."|".$impuesto->tasa_o_cuota])){
                                    $grupo_traslados[$impuesto->impuesto."|".$impuesto->tipo_factor."|".$impuesto->tasa_o_cuota]["base"] = $impuesto->base / $pago->tipo_cambio;
                                    $grupo_traslados[$impuesto->impuesto."|".$impuesto->tipo_factor."|".$impuesto->tasa_o_cuota]["impuesto"] = $impuesto->impuesto;
                                    $grupo_traslados[$impuesto->impuesto."|".$impuesto->tipo_factor."|".$impuesto->tasa_o_cuota]["tipo_factor"] = $impuesto->tipo_factor;
                                    $grupo_traslados[$impuesto->impuesto."|".$impuesto->tipo_factor."|".$impuesto->tasa_o_cuota]["tasa_o_cuota"] = $impuesto->tasa_o_cuota;
                                    $grupo_traslados[$impuesto->impuesto."|".$impuesto->tipo_factor."|".$impuesto->tasa_o_cuota]["importe"] = $impuesto->base * $impuesto->tasa_o_cuota / $pago->tipo_cambio;
                                }
                                else{
                                    $grupo_traslados[$impuesto->impuesto."|".$impuesto->tipo_factor."|".$impuesto->tasa_o_cuota]["base"] += $impuesto->base / $pago->tipo_cambio;
                                    $grupo_traslados[$impuesto->impuesto."|".$impuesto->tipo_factor."|".$impuesto->tasa_o_cuota]["importe"] += $impuesto->base * $impuesto->tasa_o_cuota / $pago->tipo_cambio;
                                }
                            }
                        }
                        else{
                            if(!isset($grupo_retenciones[$impuesto->impuesto."|".$impuesto->tipo_factor])){
                                $grupo_retenciones[$impuesto->impuesto."|".$impuesto->tipo_factor]["base"] = $impuesto->base / $pago->tipo_cambio;
                                $grupo_retenciones[$impuesto->impuesto."|".$impuesto->tipo_factor]["impuesto"] = $impuesto->impuesto;
                                $grupo_retenciones[$impuesto->impuesto."|".$impuesto->tipo_factor]["tipo_factor"] = $impuesto->tipo_factor;
                                $grupo_retenciones[$impuesto->impuesto."|".$impuesto->tipo_factor]["tasa_o_cuota"] = $impuesto->tasa_o_cuota;
                                $grupo_retenciones[$impuesto->impuesto."|".$impuesto->tipo_factor]["importe"] = $impuesto->base * $impuesto->tasa_o_cuota / $pago->tipo_cambio;
                            }
                            else{
                                $grupo_retenciones[$impuesto->impuesto."|".$impuesto->tipo_factor]["base"] += $impuesto->base / $pago->tipo_cambio;
                                $grupo_retenciones[$impuesto->impuesto."|".$impuesto->tipo_factor]["importe"] += $impuesto->base * $impuesto->tasa_o_cuota / $pago->tipo_cambio;
                            }
                        }
                    }

                    if(count($grupo_retenciones) > 0){
                        $retenciones = new CfdiNodo("pago20:RetencionesP");

                        if($grupo_retenciones) foreach($grupo_retenciones as $grupo_retencion){
                            $retencion = new CfdiNodo("pago20:RetencionP");

                            $retencion->agregarAtributo("ImpuestoP", $grupo_retencion["impuesto"]);

                            if($pago->moneda != 'MXN'){
                                $retencion->agregarAtributo("ImporteP", number_format($grupo_retencion["importe"],4,".",""));
                            }
                            else{
                                $retencion->agregarAtributo("ImporteP", number_format($grupo_retencion["importe"],2,".",""));
                            }

                            $retenciones->agregarNodo($retencion);
                        }

                        $impuestos->agregarNodo($retenciones);
                    }

                    if(count($grupo_traslados) > 0){
                        $traslados = new CfdiNodo("pago20:TrasladosP");

                        if($grupo_traslados) foreach($grupo_traslados as $grupo_traslado){
                            $traslado = new CfdiNodo("pago20:TrasladoP");
                            
                            $traslado->agregarAtributo("BaseP", number_format($grupo_traslado["base"],4,".",""));
                            $traslado->agregarAtributo("ImpuestoP", $grupo_traslado["impuesto"]);
                            $traslado->agregarAtributo("TipoFactorP", $grupo_traslado["tipo_factor"]);

                            if(strtoupper($grupo_traslado["tipo_factor"]) != "EXENTO"){
                                $traslado->agregarAtributo("TasaOCuotaP", $grupo_traslado["tasa_o_cuota"]);

                                if($pago->moneda != 'MXN'){
                                    $retencion->agregarAtributo("ImporteP", number_format($grupo_traslado["importe"],4,".",""));
                                }
                                else{
                                    $retencion->agregarAtributo("ImporteP", number_format($grupo_traslado["importe"],2,".",""));
                                }
                            }

                            $traslados->agregarNodo($traslado);
                        }

                        $impuestos->agregarNodo($traslados);
                    }

                    $nodo_pago->agregarNodo($impuestos);
                }

                $nodo->agregarNodo($nodo_pago);
            }
        }

        return $nodo;
    }
}