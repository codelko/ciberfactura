<?php

namespace Codelko\Ciberfactura\Libraries\Complementos;

use Codelko\Ciberfactura\Libraries\CfdiComplemento;
use Codelko\Ciberfactura\Libraries\CfdiNodo;
use Codelko\Ciberfactura\Models\CfdiConcepto;
use Codelko\Ciberfactura\Models\CfdiConceptoTerceros;
use Codelko\Ciberfactura\Models\CfdiImpuestoTerceros;

class Terceros extends CfdiComplemento {
    public $version = "1.1";

    public $name = "terceros";
    public $xmlns = ['xmlns:terceros' => 'http://www.sat.gob.mx/terceros'];
    public $schemaLocation = ['http://www.sat.gob.mx/terceros', 'http://www.sat.gob.mx/sitio_internet/cfd/terceros/terceros11.xsd'];

    public $terceros;
    public $concepto;
    public $impuestos;

    public function __construct(CfdiConceptoTerceros $terceros){
        $this->terceros = $terceros;
        $this->concepto = CfdiConcepto::find($terceros->cfdi_concepto_id);
        $this->impuestos = CfdiImpuestoTerceros::where("cfdi_concepto_terceros_id", $this->terceros->id)->get();
    }

    public function getNS(){
        return $this->xmlns;
    }

    public function getSchemaLocation(){
        return $this->schemaLocation;
    }

    public function getNodo(){
        $complemento = new CfdiNodo("cfdi:ComplementoConcepto");

        $nodo = new CfdiNodo("terceros:PorCuentadeTerceros");

        $nodo->agregarAtributo("version", $this->version);
        $nodo->agregarAtributo("rfc", $this->terceros->rfc);
        $nodo->agregarAtributo("nombre", $this->terceros->nombre);

        $informacion_fiscal = new CfdiNodo("terceros:InformacionFiscalTercero");

        if(isset($this->terceros->calle) && $this->terceros->calle){
            $informacion_fiscal->agregarAtributo("calle", $this->terceros->calle);
        }

        if(isset($this->terceros->no_exterior) && $this->terceros->no_exterior){
            $informacion_fiscal->agregarAtributo("noExterior", $this->terceros->no_exterior);
        }

        if(isset($this->terceros->no_interior) && $this->terceros->no_interior){
            $informacion_fiscal->agregarAtributo("noInterior", $this->terceros->no_interior);
        }

        if(isset($this->terceros->colonia) && $this->terceros->colonia){
            $informacion_fiscal->agregarAtributo("colonia", $this->terceros->colonia);
        }

        if(isset($this->terceros->localidad) && $this->terceros->localidad){
            $informacion_fiscal->agregarAtributo("localidad", $this->terceros->localidad);
        }

        if(isset($this->terceros->municipio) && $this->terceros->municipio){
            $informacion_fiscal->agregarAtributo("municipio", $this->terceros->municipio);
        }

        if(isset($this->terceros->estado) && $this->terceros->estado){
            $informacion_fiscal->agregarAtributo("estado", $this->terceros->estado);
        }

        if(isset($this->terceros->pais) && $this->terceros->pais){
            $informacion_fiscal->agregarAtributo("pais", $this->terceros->pais);
        }

        if(isset($this->terceros->codigo_postal) && $this->terceros->codigo_postal){
            $informacion_fiscal->agregarAtributo("codigoPostal", $this->terceros->codigo_postal);
        }

        $nodo->agregarNodo($informacion_fiscal);

        if(isset($this->terceros->cuenta_predial) && $this->terceros->cuenta_predial){
            $cuenta_predial = new CfdiNodo("terceros:CuentaPredial");
            $cuenta_predial->agregarAtributo("numero", $this->terceros->cuenta_predial);

            $nodo->agregarNodo($cuenta_predial);
        }

        // $parte = new CfdiNodo("terceros:Parte");

        // $parte->agregarAtributo("cantidad", $this->concepto->cantidad);
        // $parte->agregarAtributo("descripcion", $this->concepto->descripcion);
        // $parte->agregarAtributo("importe", $this->concepto->importe);
        // $parte->agregarAtributo("valorUnitario", $this->concepto->valor_unitario);

        // $nodo->agregarNodo($parte);

        $impuestos = new CfdiNodo("terceros:Impuestos");

        $traslados = new CfdiNodo("terceros:Traslados");
        $retenciones = new CfdiNodo("terceros:Retenciones");

        $cfdi_impuestos = $this->impuestos;

        if($cfdi_impuestos) foreach($cfdi_impuestos as $cfdi_impuesto){
            if($cfdi_impuesto->impuesto == "001"){
                $cfdi_impuesto->impuesto = "ISR";
            }

            if($cfdi_impuesto->impuesto == "002"){
                $cfdi_impuesto->impuesto = "IVA";
            }

            if(strtolower($cfdi_impuesto->type) == "traslado"){
                $traslado = new CfdiNodo("terceros:Traslado");

                $traslado->agregarAtributo("impuesto", $cfdi_impuesto->impuesto);

                if($cfdi_impuesto->tipo_factor == "Tasa" || $cfdi_impuesto->tipo_factor == "Cuota"){
                    $traslado->agregarAtributo("tasa", $cfdi_impuesto->tasa_o_cuota);
                    $traslado->agregarAtributo("importe", number_format($cfdi_impuesto->importe,2,".",""));
                }

                $traslados->agregarNodo($traslado);
            }

            if(strtolower($cfdi_impuesto->type) == "retencion"){
                $retencion = new CfdiNodo("terceros:Retencion");

                $retencion->agregarAtributo("impuesto", $cfdi_impuesto->impuesto);

                if($cfdi_impuesto->tipo_factor == "Tasa" || $cfdi_impuesto->tipo_factor == "Cuota"){
                    $retencion->agregarAtributo("tasa", $cfdi_impuesto->tasa_o_cuota);
                    $retencion->agregarAtributo("importe", number_format($cfdi_impuesto->importe,2,".",""));
                }

                $retenciones->agregarNodo($retencion);
            }
        }

        if(count($traslados->nodos) > 0){
            $impuestos->agregarNodo($traslados);
        }

        if(count($retenciones->nodos) > 0){
            $impuestos->agregarNodo($retenciones);
        }

        $nodo->agregarNodo($impuestos);

        $complemento->agregarNodo($nodo);

        return $complemento;
    }
}