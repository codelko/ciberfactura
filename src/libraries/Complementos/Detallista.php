<?php

namespace Codelko\Ciberfactura\Libraries\Complementos;

use Codelko\Ciberfactura\Libraries\CfdiComplemento;
use Codelko\Ciberfactura\Libraries\CfdiNodo;

class Detallista extends CfdiComplemento {
    private $type = "SimpleInvoiceType";
    private $document_version = "AMC8.1";
    private $document_status = "ORIGINAL";
    private $content_version = "1.3.1";
    private $entity_type = "INVOICE";
    private $code = "ZZZ";

    public $name = "detallista";
    public $xmlns = ['xmlns:detallista' => 'http://www.sat.gob.mx/detallista'];
    public $schemaLocation = ['http://www.sat.gob.mx/detallista', 'http://www.sat.gob.mx/sitio_internet/cfd/detallista/detallista.xsd'];

    public $importe;
    public $pedido;
    public $contrarecibo;
    public $no_proveedor;
    public $gln_receptor;
    public $gln_emisor;

    public function __construct($importe, $pedido, $contrarecibo, $no_proveedor, $gln_emisor, $gln_receptor){
        $this->entidades = [];

        $this->importe = $importe;
        $this->pedido = $pedido;
        $this->contrarecibo = $contrarecibo;
        $this->no_proveedor = $no_proveedor;
        $this->gln_emisor = $gln_emisor;
        $this->gln_receptor = $gln_receptor;
    }

    public function getNS(){
        return $this->xmlns;
    }

    public function getSchemaLocation(){
        return $this->schemaLocation;
    }

    public function getNodo(){
        $nodo = new CfdiNodo("detallista:detallista");

        $nodo->agregarAtributo("type", $this->type);
        $nodo->agregarAtributo("contentVersion", $this->content_version);
        $nodo->agregarAtributo("documentStatus", $this->document_status);
        $nodo->agregarAtributo("documentStructureVersion", $this->document_version);

        $nodo1 = new CfdiNodo("detallista:requestForPaymentIdentification");
        $nodo1_1 = new CfdiNodo("detallista:entityType", $this->entity_type);
        $nodo1->agregarNodo($nodo1_1);

        $nodo->agregarNodo($nodo1);

        $nodo2 = new CfdiNodo("detallista:specialInstruction");
        $nodo2->agregarAtributo("code", $this->code);
        $nodo2_1 = new CfdiNodo("detallista:text", $this->importe);
        $nodo2->agregarNodo($nodo2_1);

        $nodo->agregarNodo($nodo2);

        $nodo3 = new CfdiNodo("detallista:orderIdentification");
        $nodo3_1 = new CfdiNodo("detallista:referenceIdentification", $this->pedido);
        $nodo3_1->agregarAtributo("type", "ON");
        $nodo3->agregarNodo($nodo3_1);

        $nodo->agregarNodo($nodo3);

        $nodo4 = new CfdiNodo("detallista:AdditionalInformation");
        $nodo4_1 = new CfdiNodo("detallista:referenceIdentification", "0");
        $nodo4_1->agregarAtributo("type", "ACE");
        $nodo4->agregarNodo($nodo4_1);

        $nodo->agregarNodo($nodo4);

        $nodo5 = new CfdiNodo("detallista:DeliveryNote");
        $nodo5_1 = new CfdiNodo("detallista:referenceIdentification", $this->contrarecibo);
        $nodo5->agregarNodo($nodo5_1);

        $nodo->agregarNodo($nodo5);

        $nodo6 = new CfdiNodo("detallista:buyer");
        $nodo6_1 = new CfdiNodo("detallista:gln", $this->gln_emisor);
        $nodo6->agregarNodo($nodo6_1);

        $nodo->agregarNodo($nodo6);

        $nodo7 = new CfdiNodo("detallista:seller");
        $nodo7_1 = new CfdiNodo("detallista:gln", $this->gln_receptor);
        $nodo7_2 = new CfdiNodo("detallista:alternatePartyIdentification", $this->no_proveedor);
        $nodo7_2->agregarAtributo("type", "SELLER_ASSIGNED_IDENTIFIER_FOR_A_PARTY");
        $nodo7->agregarNodo($nodo7_1);
        $nodo7->agregarNodo($nodo7_2);

        $nodo->agregarNodo($nodo7);

        return $nodo;
    }
}