<?php

namespace Codelko\Ciberfactura\Libraries\Complementos;

use Codelko\Ciberfactura\Libraries\CfdiComplemento;
use Codelko\Ciberfactura\Libraries\CfdiNodo;
use Codelko\Ciberfactura\Models\Pagos\CfdiPago;
use Codelko\Ciberfactura\Models\Pagos\CfdiPagoImpuesto;
use Codelko\Ciberfactura\Models\Pagos\CfdiPagoRelacion;

class Pago extends CfdiComplemento {
    public $version = "1.0";

    public $name = "pago10";
    public $xmlns = ['xmlns:pago10' => 'http://www.sat.gob.mx/Pagos'];
    public $schemaLocation = ['http://www.sat.gob.mx/Pagos', 'http://www.sat.gob.mx/sitio_internet/cfd/Pagos/Pagos10.xsd'];

    public $pagos;
    public $monto = 0;

    public function __construct(CfdiPago $pago){
        $this->pagos[] = $pago;
        $this->monto = $pago->monto;
    }

    public function getNS(){
        return $this->xmlns;
    }

    public function getSchemaLocation(){
        return $this->schemaLocation;
    }

    public function addPago(CfdiPago $pago){
        $this->pagos[] = $pago;
        $this->monto += $pago->monto;
    }

    public function addRelacion(CfdiPagoRelacion $relacion){
        //$this->relaciones[] = $relacion;
    }

    public function addImpuesto(CfdiPagoImpuesto $impuesto){
        //$this->impuestos[] = $impuesto;
    }

    public function getNodo(){
        $nodo = new CfdiNodo("pago10:Pagos");

        $nodo->agregarAtributo("Version", $this->version);

        if($this->pagos && count($this->pagos) > 0){
            foreach($this->pagos as $pago){
                $nodo_pago = new CfdiNodo("pago10:Pago");

                $nodo_pago->agregarAtributo("FechaPago", $pago->fecha_pago);
                $nodo_pago->agregarAtributo("FormaDePagoP", $pago->forma_pago);
                $nodo_pago->agregarAtributo("MonedaP", $pago->moneda);

                if($pago->moneda != 'MXN'){
                    $nodo_pago->agregarAtributo("TipoCambioP", $pago->tipo_cambio);
                }

                $nodo_pago->agregarAtributo("Monto", number_format($pago->monto,2,".",""));

                if($pago->num_operacion){
                    $nodo_pago->agregarAtributo("NumOperacion", $pago->num_operacion);
                }

                if($pago->rfc_emisor_cta_ord){
                    $nodo_pago->agregarAtributo("RfcEmisorCtaOrd", $pago->rfc_emisor_cta_ord);
                }

                if($pago->nom_banco_ord_ext){
                    $nodo_pago->agregarAtributo("NomBancoOrdExt", $pago->nom_banco_ord_ext);
                }

                if($pago->cta_ordenante){
                    $nodo_pago->agregarAtributo("CtaOrdenante", $pago->cta_ordenante);
                }

                if($pago->rfc_emisor_cta_ben){
                    $nodo_pago->agregarAtributo("RfcEmisorCtaBen", $pago->rfc_emisor_cta_ben);
                }

                if($pago->cta_beneficiario){
                    $nodo_pago->agregarAtributo("CtaBeneficiario", $pago->cta_beneficiario);
                }

                if($pago->relaciones){
                    foreach($pago->relaciones as $relacion){
                        $rel_nodo = new CfdiNodo("pago10:DoctoRelacionado");

                        if($relacion->id_documento){
                            $rel_nodo->agregarAtributo("IdDocumento", $relacion->id_documento);
                        }

                        if($relacion->serie){
                            $rel_nodo->agregarAtributo("Serie", $relacion->serie);
                        }

                        if($relacion->folio){
                            $rel_nodo->agregarAtributo("Folio", $relacion->folio);
                        }

                        if($relacion->moneda){
                            $rel_nodo->agregarAtributo("MonedaDR", $relacion->moneda);
                        }

                        if($relacion->moneda != "MXN" && $pago->moneda != $relacion->moneda){
                            $rel_nodo->agregarAtributo("TipoCambioDR", $relacion->tipo_cambio);
                        }

                        if($relacion->metodo_pago){
                            $rel_nodo->agregarAtributo("MetodoDePagoDR", $relacion->metodo_pago);
                        }

                        if($relacion->num_parcialidad){
                            $rel_nodo->agregarAtributo("NumParcialidad", $relacion->num_parcialidad);
                        }
                        
                        $rel_nodo->agregarAtributo("ImpSaldoAnt", number_format($relacion->importe_saldo_anterior,2,".",""));
                        $rel_nodo->agregarAtributo("ImpPagado", number_format($relacion->importe_pagado,2,".",""));
                        $rel_nodo->agregarAtributo("ImpSaldoInsoluto", number_format($relacion->importe_saldo_insoluto,2,".",""));

                        $nodo_pago->agregarNodo($rel_nodo);
                    }
                }

                if($pago->impuestos){
                    $impuestos = new CfdiNodo("pago10:Impuestos");

                    $traslados = false;
                    $retenciones = false;

                    $total_traslados = 0;
                    $total_retenciones = 0;

                    foreach($pago->impuestos as $impuesto){
                        if($impuesto->type == "TRASLADO"){
                            if(!$traslados){
                                $traslados = new CfdiNodo("pago10:Traslados");
                            }

                            $traslado = new CfdiNodo("pago10:Traslado");

                            $traslado->agregarAtributo("Impuesto", $impuesto->impuesto);
                            $traslado->agregarAtributo("TipoFactor", $impuesto->tipo_factor);
                            $traslado->agregarAtributo("TasaOCuota", $impuesto->tasa_o_cuota);
                            $traslado->agregarAtributo("Importe", $impuesto->importe);

                            $traslados->agregarNodo($traslado);

                            $total_traslados += $impuesto->importe;
                        }
                        else{
                            if(!$retenciones){
                                $retenciones = new CfdiNodo("pago10:Retenciones");
                            }

                            $retencion = new CfdiNodo("pago10:Retencion");

                            $retencion->agregarAtributo("Impuesto", $impuesto->impuesto);
                            $retencion->agregarAtributo("Importe", $impuesto->importe);

                            $retenciones->agregarNodo($retencion);

                            $total_retenciones += $impuesto->importe;
                        }
                    }

                    $impuestos->agregarAtributo("TotalImpuestosRetenidos", $total_retenciones);
                    $impuestos->agregarAtributo("TotalImpuestosTrasladados", $total_traslados);

                    if($retenciones){
                        $impuestos->agregarNodo($retenciones);
                    }
                    if($traslados){
                        $impuestos->agregarNodo($traslados);
                    }

                    //Actualmente la Guia de Llenado dice que no se debe agregar el nodo impuestos.
                    //$nodo->agregarNodo($impuestos);
                }

                $nodo->agregarNodo($nodo_pago);
            }
        }

        return $nodo;
    }
}