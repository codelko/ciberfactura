<?php

namespace Codelko\Ciberfactura\Libraries\Complementos;

use Codelko\Ciberfactura\Libraries\CfdiComplemento;
use Codelko\Ciberfactura\Libraries\CfdiNodo;
use Codelko\Ciberfactura\Models\Nomina\CfdiNomina;
use Codelko\Ciberfactura\Models\Pagos\CfdiPago;
use Codelko\Ciberfactura\Models\Pagos\CfdiPagoImpuesto;
use Codelko\Ciberfactura\Models\Pagos\CfdiPagoRelacion;

class Nomina extends CfdiComplemento {
    public $version = "1.2";

    public $name = "nomina12";
    public $xmlns = ['xmlns:nomina12' => 'http://www.sat.gob.mx/nomina12'];
    public $schemaLocation = ['http://www.sat.gob.mx/nomina12', 'http://www.sat.gob.mx/sitio_internet/cfd/nomina/nomina12.xsd'];
    public $tipo_nomina = 'O';
    public $fecha_pago;

    public $fecha_inicial_pago;
    public $fecha_final_pago;
    public $dias_pagados;

    public $total_percepciones;
    public $total_deducciones;
    public $total_otros_pagos;

    public $registro_patronal;
    public $curp_patronal;

    public $curp;
    public $numero_empleado;
    public $nss;
    public $rfc;
    public $puesto;
    public $riesgo_puesto;
    public $salario_base;
    public $salario_integrado;
    public $fecha_contratacion;
    public $antiguedad;
    public $periodicidad;
    public $tipo_jornada;
    public $tipo_contrato;
    public $tipo_regimen;
    public $banco;
    public $clabe;

    public $nomina;

    public function __construct(CfdiNomina $nomina){
        $this->fecha_pago = $nomina->fecha_pago;

        $this->fecha_inicial_pago = $nomina->fecha_inicial;
        $this->fecha_final_pago = $nomina->fecha_final;
        $this->dias_pagados = $nomina->dias_trabajados;

        $this->total_percepciones = $nomina->total_percepciones;
        $this->total_deducciones = $nomina->total_deducciones;
        $this->total_otros_pagos = $nomina->total_otros_pagos;

        $this->registro_patronal = $nomina->registro_patronal;
        $this->curp_patronal = $nomina->curp_patronal;

        $this->curp = $nomina->curp;
        $this->numero_empleado = $nomina->numero_empleado;
        $this->nss = $nomina->nss;
        $this->rfc = $nomina->rfc;
        $this->puesto = $nomina->puesto;
        $this->riesgo_puesto = $nomina->riesgo_puesto;
        $this->salario_base = $nomina->salario_base;
        $this->salario_integrado = $nomina->salario_integrado;
        $this->fecha_contratacion = $nomina->fecha_contratacion;
        $this->antiguedad = $nomina->antiguedad;
        $this->tipo_regimen = $nomina->tipo_regimen;
        $this->periodicidad = $this->getPeriodicidad($nomina->periodicidad);
        $this->tipo_jornada = $this->getJornada($nomina->tipo_jornada);
        $this->tipo_contrato = $this->getContrato($nomina->tipo_contrato);
        $this->banco = $nomina->banco;
        $this->clabe = $nomina->clabe;

        $this->nomina = $nomina;
    }

    public function getNS(){
        return $this->xmlns;
    }

    public function getSchemaLocation(){
        return $this->schemaLocation;
    }

    public function getNodo(){
        $nodo = new CfdiNodo("nomina12:Nomina");

        $nodo->agregarAtributo("Version", $this->version);

        $nodo->agregarAtributo("FechaPago", $this->fecha_pago);
        $nodo->agregarAtributo("FechaInicialPago", $this->fecha_inicial_pago);
        $nodo->agregarAtributo("FechaFinalPago", $this->fecha_final_pago);
        $nodo->agregarAtributo("NumDiasPagados", $this->dias_pagados);
        $nodo->agregarAtributo("TipoNomina", $this->tipo_nomina);

        $nodo_emisor = new CfdiNodo("nomina12:Emisor");
        if($this->tipo_contrato != "09" && $this->tipo_contrato != "10" && $this->tipo_contrato != "99"){
            $nodo_emisor->agregarAtributo("RegistroPatronal", $this->registro_patronal);
        }

        //Agregada la Curp para Nominas de Personas Físicas
        if(isset($this->curp_patronal) && $this->curp_patronal){
            $nodo_emisor->agregarAtributo("Curp", $this->curp_patronal);
        }

        $nodo->agregarNodo($nodo_emisor);

        $nodo_receptor = new CfdiNodo("nomina12:Receptor");

        $nodo_receptor->agregarAtributo("Curp", $this->curp);
        $nodo_receptor->agregarAtributo("TipoContrato", $this->tipo_contrato);
        //$nodo_receptor->agregarAtributo("TipoJornada", $this->tipo_jornada);
        $nodo_receptor->agregarAtributo("TipoRegimen", $this->tipo_regimen);
        $nodo_receptor->agregarAtributo("NumEmpleado", $this->numero_empleado);
        $nodo_receptor->agregarAtributo("PeriodicidadPago", $this->periodicidad);
        $nodo_receptor->agregarAtributo("ClaveEntFed", "JAL");
        $nodo_receptor->agregarAtributo("NumSeguridadSocial", $this->nss);

        if(strlen($this->clabe) == 18){
            if($this->clabe){
                $nodo_receptor->agregarAtributo("CuentaBancaria", $this->clabe);
            }
        }
        elseif(strlen($this->clabe) == 16 || strlen($this->clabe) == 11 || strlen($this->clabe) == 10){
            if($this->banco){
                $nodo_receptor->agregarAtributo("Banco", $this->banco);
            }

            if($this->clabe){
                $nodo_receptor->agregarAtributo("CuentaBancaria", $this->clabe);
            }
        }
        else{
            if($this->banco){
                $nodo_receptor->agregarAtributo("Banco", $this->banco);
            }
        }

        $nodo_receptor->agregarAtributo("FechaInicioRelLaboral", $this->fecha_contratacion);
        $nodo_receptor->agregarAtributo("Antigüedad", $this->antiguedad);

        if($this->puesto){
            $nodo_receptor->agregarAtributo("Puesto", $this->puesto);
        }

        $nodo_receptor->agregarAtributo("SalarioBaseCotApor", $this->salario_base);
        $nodo_receptor->agregarAtributo("RiesgoPuesto", $this->riesgo_puesto);
        $nodo_receptor->agregarAtributo("SalarioDiarioIntegrado", $this->salario_integrado);

        $nodo->agregarNodo($nodo_receptor);

        $nodo_percepciones = new CfdiNodo("nomina12:Percepciones");
        $otros_pagos = new CfdiNodo("nomina12:OtrosPagos");

        $subsidio = false;

        if($this->nomina->perceptions){
            $total_gravado = 0;
            $total_exento = 0;
            $total_sueldos = 0;

            foreach($this->nomina->perceptions as $perception){
                if(str_pad($perception->clave,3,"0", STR_PAD_LEFT) != '017' && str_pad($perception->clave,3,"0", STR_PAD_LEFT) != '050'){
                    $nodo_percepcion = new CfdiNodo("nomina12:Percepcion");

                    $nodo_percepcion->agregarAtributo("TipoPercepcion", $perception->tipo);
                    $nodo_percepcion->agregarAtributo("Clave", $perception->clave);
                    $nodo_percepcion->agregarAtributo("Concepto", $perception->concepto);
                    $nodo_percepcion->agregarAtributo("ImporteGravado", number_format($perception->importe_gravado,2,".",""));
                    $nodo_percepcion->agregarAtributo("ImporteExento", number_format($perception->importe_exento,2,".",""));

                    $total_gravado += $perception->importe_gravado;
                    $total_exento += $perception->importe_exento;

                    if($perception->clave != "022" && $perception->clave != "023" && $perception->clave != "025" && $perception->clave != "039" && $perception->clave != "044"){
                        $total_sueldos += $perception->importe_gravado + $perception->importe_exento;
                    }

                    $nodo_percepciones->agregarNodo($nodo_percepcion);
                }
                else{
                    $nodo_otro_pago = new CfdiNodo("nomina12:OtroPago");

                    //Subsidio al Empleo
                    if($this->tipo_regimen == "02" && str_pad($perception->clave,3,"0", STR_PAD_LEFT) == '017'){
                        $nodo_otro_pago->agregarAtributo("TipoOtroPago", '002');
                        $nodo_otro_pago->agregarAtributo("Clave", '002');

                        $nodo_subsidio = new CfdiNodo("nomina12:SubsidioAlEmpleo");

                        $nodo_subsidio->agregarAtributo("SubsidioCausado", number_format($perception->importe_gravado + $perception->importe_exento,2,".",""));

                        $nodo_otro_pago->agregarNodo($nodo_subsidio);

                        $subsidio = true;
                    }

                    //Viaticos
                    if(str_pad($perception->clave,3,"0", STR_PAD_LEFT) == '050'){
                        $nodo_otro_pago->agregarAtributo("TipoOtroPago", '003');
                        $nodo_otro_pago->agregarAtributo("Clave", '003');
                    }

                    $nodo_otro_pago->agregarAtributo("Concepto", $perception->concepto);
                    $nodo_otro_pago->agregarAtributo("Importe", number_format($perception->importe_gravado + $perception->importe_exento,2,".",""));

                    $otros_pagos->agregarNodo($nodo_otro_pago);
                }
            }

            //Si no se registro un subsidio al empleo
            if(!$subsidio && $this->tipo_regimen == "02"){
                $nodo_otro_pago = new CfdiNodo("nomina12:OtroPago");
    
                $nodo_otro_pago->agregarAtributo("TipoOtroPago", '002');
                $nodo_otro_pago->agregarAtributo("Clave", '002');
    
                $nodo_subsidio = new CfdiNodo("nomina12:SubsidioAlEmpleo");
    
                $nodo_subsidio->agregarAtributo("SubsidioCausado", number_format(0.01,2,".",""));
    
                $nodo_otro_pago->agregarNodo($nodo_subsidio);

                $nodo_otro_pago->agregarAtributo("Concepto", "Subsidio para el empleo (efectivamente entregado al trabajador).");
                $nodo_otro_pago->agregarAtributo("Importe", number_format(0.01,2,".",""));

                $this->total_otros_pagos = 0.01;

                $otros_pagos->agregarNodo($nodo_otro_pago);
    
                $subsidio = true;
            }

            $nodo_percepciones->agregarAtributo("TotalGravado", number_format($total_gravado,2,".",""));
            $nodo_percepciones->agregarAtributo("TotalExento", number_format($total_exento,2,".",""));
            $nodo_percepciones->agregarAtributo("TotalSueldos", number_format($total_sueldos,2,".",""));

            $nodo->agregarNodo($nodo_percepciones);

            $nodo->agregarAtributo("TotalPercepciones", number_format($this->total_percepciones,2,".",""));
            $nodo->agregarAtributo("TotalOtrosPagos", number_format($this->total_otros_pagos,2,".",""));
        }

        $nodo_deducciones = new CfdiNodo("nomina12:Deducciones");

        if($this->nomina->deductions && count($this->nomina->deductions) > 0){
            $total_otras_deducciones = 0;
            $total_impuestos_retenidos = 0;

            foreach($this->nomina->deductions as $deduction){
                $nodo_deduccion = new CfdiNodo("nomina12:Deduccion");

                $nodo_deduccion->agregarAtributo("TipoDeduccion", $deduction->tipo);
                $nodo_deduccion->agregarAtributo("Clave", $deduction->clave);
                $nodo_deduccion->agregarAtributo("Concepto", $deduction->concepto);
                $nodo_deduccion->agregarAtributo("Importe", number_format($deduction->importe_gravado + $deduction->importe_exento,2,".",""));

                if($deduction->clave == "002"){
                    $total_impuestos_retenidos += $deduction->importe_gravado + $deduction->importe_exento;
                }
                else{
                    $total_otras_deducciones += $deduction->importe_gravado + $deduction->importe_exento;
                }

                $nodo_deducciones->agregarNodo($nodo_deduccion);
            }

            $nodo_deducciones->agregarAtributo("TotalOtrasDeducciones", number_format($total_otras_deducciones,2,".",""));
            if($total_impuestos_retenidos > 0){
                $nodo_deducciones->agregarAtributo("TotalImpuestosRetenidos", number_format($total_impuestos_retenidos,2,".",""));
            }

            $nodo->agregarNodo($nodo_deducciones);

            $nodo->agregarAtributo("TotalDeducciones", number_format($this->total_deducciones,2,".",""));
        }

        if($otros_pagos->tieneNodos()){
            $nodo->agregarNodo($otros_pagos);
        }

        return $nodo;
    }

    public function getPeriodicidad($periodicidad){
        switch(strtoupper($periodicidad)){
            case "DIARIO": return "01";
            case "SEMANAL": return "02";
            case "CATORCENAL": return "03";
            case "QUINCENAL": return "04";
            case "MENSUAL": return "05";
            case "BIMESTRAL": return "06";
        }
    }

    public function getContrato($contrato){
        switch(strtoupper($contrato)){
            case "BASE": return "01";
            case "EVENTUAL": return "04";
            case "CONFIANZA": return "03";
            case "SINDICALIZADO": return "01";
            case "A PRUEBA": return "05";
            case "OTRO": return "99";
        }
    }

    public function getJornada($jornada){
        switch(strtoupper($jornada)){
            case "TIEMPO COMPLETO": return "01";
            case "MEDIO TIEMPO": return "08";
            case "DIURNA": return "01";
            case "NOCTURNA": return "02";
            case "MIXTA": return "03";
            case "POR HORA": return "04";
            case "REDUCIDA": return "05";
            case "POR TURNOS": return "08";
            case "OTROS": return "99";
        }
    }
}