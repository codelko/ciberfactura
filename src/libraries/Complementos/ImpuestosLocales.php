<?php

namespace Codelko\Ciberfactura\Libraries\Complementos;

use Codelko\Ciberfactura\Libraries\CfdiComplemento;
use Codelko\Ciberfactura\Libraries\CfdiNodo;

class ImpuestosLocales extends CfdiComplemento {
    public $version = "1.0";

    public $name = "implocal";
    public $xmlns = ['xmlns:implocal' => 'http://www.sat.gob.mx/implocal'];
    public $schemaLocation = ['http://www.sat.gob.mx/implocal', 'http://www.sat.gob.mx/sitio_internet/cfd/implocal/implocal.xsd'];

    public $total_retenciones = 0;
    public $total_traslados = 0;

    public $retenciones;
    public $traslados;

    public function __construct(){
        $this->retenciones = [];
        $this->traslados = [];
    }

    public function addTax($type, $name, $rate, $total){
        if(strtoupper($type) == "TRASLADO"){
            $tax = new \stdClass();

            $tax->name = $name;
            $tax->rate = $rate;
            $tax->total = $total;

            $this->traslados[] = $tax;

            $this->total_traslados += $total;
        }
        else{
            $tax = new \stdClass();

            $tax->name = $name;
            $tax->rate = $rate;
            $tax->total = $total;

            $this->retenciones[] = $tax;

            $this->total_retenciones += $total;
        }
    }

    public function getNS(){
        return $this->xmlns;
    }

    public function getSchemaLocation(){
        return $this->schemaLocation;
    }

    public function getNodo(){
        $nodo = new CfdiNodo("implocal:ImpuestosLocales");

        $nodo->agregarAtributo("version", $this->version);
        $nodo->agregarAtributo("TotaldeRetenciones", number_format($this->total_retenciones,2,".",""));
        $nodo->agregarAtributo("TotaldeTraslados", number_format($this->total_traslados,2,".",""));

        if($this->retenciones) foreach($this->retenciones as $retencion){
            $ret_nodo = new CfdiNodo("implocal:RetencionesLocales");

            $ret_nodo->agregarAtributo("ImpLocRetenido", $retencion->name);
            $ret_nodo->agregarAtributo("TasadeRetencion", number_format($retencion->rate,2,".",""));
            $ret_nodo->agregarAtributo("Importe", number_format($retencion->total,2,".",""));

            $nodo->agregarNodo($ret_nodo);
        }

        if($this->traslados) foreach($this->traslados as $traslado){
            $tra_nodo = new CfdiNodo("implocal:TrasladosLocales");

            $tra_nodo->agregarAtributo("ImpLocTrasladado", $traslado->name);
            $tra_nodo->agregarAtributo("TasadeTraslado", number_format($traslado->rate,2,".",""));
            $tra_nodo->agregarAtributo("Importe", number_format($traslado->total,2,".",""));

            $nodo->agregarNodo($tra_nodo);
        }

        return $nodo;
    }
}