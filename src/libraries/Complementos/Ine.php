<?php

namespace Codelko\Ciberfactura\Libraries\Complementos;

use Codelko\Ciberfactura\Libraries\CfdiComplemento;
use Codelko\Ciberfactura\Libraries\CfdiNodo;

class Ine extends CfdiComplemento {
    public $version = "1.1";

    public $name = "ine";
    public $xmlns = ['xmlns:ine' => 'http://www.sat.gob.mx/ine'];
    public $schemaLocation = ['http://www.sat.gob.mx/ine', 'http://www.sat.gob.mx/sitio_internet/cfd/ine/ine11.xsd'];

    public $entidades;

    private $tipo_proceso;
    private $tipo_comite;
    private $id_contabilidad;

    public function __construct($tipo_proceso, $tipo_comite = '', $id_contabilidad = ''){
        $this->entidades = [];

        $this->tipo_proceso = $tipo_proceso;
        $this->tipo_comite = $tipo_comite;
        $this->id_contabilidad = $id_contabilidad;
    }

    public function addEntidad($clave, $ambito = '', $id_contabilidad = []){
        $entidad = new \stdClass();

        $entidad->clave = $clave;

        if($ambito){
            $entidad->ambito = $ambito;
        }

        if(!is_array($id_contabilidad)){
            $entidad->id_contabilidad[0] = $id_contabilidad;
        }
        else{
            $entidad->id_contabilidad = $id_contabilidad;
        }

        $this->entidades[] = $entidad;
    }

    public function getNS(){
        return $this->xmlns;
    }

    public function getSchemaLocation(){
        return $this->schemaLocation;
    }

    public function getNodo(){
        $nodo = new CfdiNodo("ine:INE");

        $nodo->agregarAtributo("Version", $this->version);
        $nodo->agregarAtributo("TipoProceso", $this->tipo_proceso);

        if(strtolower($this->tipo_proceso) == "ordinario"){
            $nodo->agregarAtributo("TipoComite", $this->tipo_comite);

            if(strtolower($this->tipo_comite) == "ejecutivo nacional"){
                $nodo->agregarAtributo("IdContabilidad", $this->id_contabilidad);
            }
            else{
                if(strtolower($this->tipo_comite) == "ejecutivo estatal"){
                    if($this->entidades) foreach($this->entidades as $entidad){
                        $nodo_entidad = new CfdiNodo("ine:Entidad");
                        $nodo_entidad->agregarAtributo("ClaveEntidad", $entidad->clave);

                        if($entidad->id_contabilidad) foreach ($entidad->id_contabilidad as $id){
                            $nodo_contabilidad = new CfdiNodo("ine:Contabilidad");

                            $nodo_contabilidad->agregarAtributo("IdContabilidad", $id);

                            $nodo_entidad->agregarNodo($nodo_contabilidad);
                        }

                        $nodo->agregarNodo($nodo_entidad);
                    }
                }
                else{
                    $nodo->agregarAtributo("IdContabilidad", $this->id_contabilidad);

                    if($this->entidades) foreach($this->entidades as $entidad) {
                        $nodo_entidad = new CfdiNodo("ine:Entidad");
                        $nodo_entidad->agregarAtributo("ClaveEntidad", $entidad->clave);

                        $nodo->agregarNodo($nodo_entidad);
                    }
                }
            }
        }
        else{
            if($this->entidades) foreach($this->entidades as $entidad) {
                $nodo_entidad = new CfdiNodo("ine:Entidad");
                $nodo_entidad->agregarAtributo("ClaveEntidad", $entidad->clave);
                $nodo_entidad->agregarAtributo("Ambito", $entidad->ambito);

                if($entidad->id_contabilidad) foreach ($entidad->id_contabilidad as $id){
                    $nodo_contabilidad = new CfdiNodo("ine:Contabilidad");

                    $nodo_contabilidad->agregarAtributo("IdContabilidad", $id);

                    $nodo_entidad->agregarNodo($nodo_contabilidad);
                }

                $nodo->agregarNodo($nodo_entidad);
            }
        }

        return $nodo;
    }
}