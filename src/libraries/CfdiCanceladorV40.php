<?php
namespace Codelko\Ciberfactura\Libraries;

use Codelko\Ciberfactura\Models\CfdiTimbre;
use Illuminate\Support\Facades\Config;
use SWServices\Authentication\AuthenticationService as Authentication;
use SWServices\Cancelation\CancelationService;
use Illuminate\Support\Str;

class CfdiCanceladorV40 implements CfdiCanceladoInterface {
    public $production;

    public $endpoint;
    public $usuario;
    public $password;

    public $no_certificado;
    public $certificado;
    public $key;
    public $rfc;

    public function __construct($rfc, $certificate = false){
        $this->production = Config::get('packages.codelko.ciberfactura.config.production');

        $this->rfc = $rfc;

        //Load Configuration Params
        if($this->production){
            $this->endpoint = Config::get('packages.codelko.ciberfactura.config.wsdl.production.endpoint');
            $this->usuario = Config::get('packages.codelko.ciberfactura.config.wsdl.production.usuario');
            $this->password = Config::get('packages.codelko.ciberfactura.config.wsdl.production.password');
        }
        else{
            $this->endpoint = Config::get('packages.codelko.ciberfactura.config.wsdl.sandbox.endpoint');
            $this->usuario = Config::get('packages.codelko.ciberfactura.config.wsdl.sandbox.usuario');
            $this->password = Config::get('packages.codelko.ciberfactura.config.wsdl.sandbox.password');
        }

        if(!$this->endpoint){
            throw new CfdiException("No se ha definido la variable de entorno y/o configuración para el endpoint del PAC Smarter Web.");
        }

        if(!$this->usuario){
            throw new CfdiException("No se ha definido la variable de entorno y/o configuración para el usuario del PAC Smarter Web.");
        }

        if(!$this->password){
            throw new CfdiException("No se ha definido la variable de entorno y/o configuración para el password del PAC Smarter Web.");
        }

        $params = [
            "url" => $this->endpoint,
            "user" => $this->usuario,
            "password"=> $this->password
        ];

        try{
            $auth = Authentication::auth($params);

            $token = $auth::Token();
            $token = $token->data;
            $token = isset($token->token) ? $token->token : '';

            $this->token = $token;
        }
        catch(\Exception $e){
            throw new CfdiException($e->getMessage());
        }

        //Load Certificate
        if($certificate){
            $url_cer = $certificate["cer"];
            $url_key = $certificate["key"];
            $clave_privada = $certificate["password"];
        }
        else{
            $url_cer = str_replace("\\","/", base_path())."/".Config::get('packages.codelko.ciberfactura.config.certificate.sandbox.cer');
            $url_key = str_replace("\\","/", base_path())."/".Config::get('packages.codelko.ciberfactura.config.certificate.sandbox.key');
            $clave_privada = Config::get('packages.codelko.ciberfactura.config.certificate.sandbox.password');
        }

        if(!$url_cer || !$url_key || !$clave_privada){
            throw new CfdiException("La configuración del certificado de sello digital no ha sido definida.");
        }

        if(!file_exists($url_cer) || !is_file($url_cer)){
            throw new CfdiException("El archivo (.cer) del Certificado de Sello Digital no fue encontrado. [$url_cer]");
        }

        if(!file_exists($url_key) || !is_file($url_key)){
            throw new CfdiException("El archivo (.key) del Certificado de Sello Digital no fue encontrado. [$url_key]");
        }

        $this->no_certificado = CfdiBase::getSerialFromCertificate( $url_cer );
        $this->certificado = CfdiBase::getCertificate( $url_cer, false );
        $this->key = CfdiBase::getPrivateKey($url_key, $clave_privada);

        $this->cer = base64_encode(file_get_contents($url_cer));
        $this->key = base64_encode(file_get_contents($url_key));
        $this->clave_privada = $clave_privada;
    }

    public function cancelar($uuid, $motivo, $folio_sustitucion = false){
        $params = array(
            "url" => $this->endpoint,
            "token" => $this->token,
            "uuid"=> $uuid,
            "password"=> $this->clave_privada,
            "rfc"=> $this->rfc,
            'b64Cer' => $this->cer,
            'b64Key' => $this->key,
            'motivo' => $motivo
        );

        if($folio_sustitucion){
            $params["folioSustitucion"] = $folio_sustitucion;
        }

        try {
            $cancelationService = CancelationService::Set($params);
            $response = $cancelationService::CancelationByCSD();

            return $response;
        } catch(\Exception $e) {
            throw new CfdiException($e->getMessage());
        }
    }
}