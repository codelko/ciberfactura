<?php
    namespace Codelko\Ciberfactura\Libraries;

    use Codelko\Ciberfactura\Libraries\Complementos\TimbreFiscalDigital;
    use Illuminate\Support\Facades\Config;
    use Illuminate\Support\Str;
    use SWServices\Authentication\AuthenticationService as Authentication;
    use SWServices\Stamp\StampService as StampService;

    class CfdiTimbrador implements CfdiTimbradoInterface{
        private $production;

        private $usuario;
        private $password;
        private $token;

        private $cer;
        private $key;
        private $clave_privada;
        private $rfc;

        public function __construct($rfc, $certificate){
            $this->production = Config::get('packages.codelko.ciberfactura.config.production');

            if($this->production){
                $this->endpoint = Config::get('packages.codelko.ciberfactura.config.wsdl.production.endpoint');
                $this->usuario = Config::get('packages.codelko.ciberfactura.config.wsdl.production.usuario');
                $this->password = Config::get('packages.codelko.ciberfactura.config.wsdl.production.password');
            }
            else{
                $this->endpoint = Config::get('packages.codelko.ciberfactura.config.wsdl.sandbox.endpoint');
                $this->usuario = Config::get('packages.codelko.ciberfactura.config.wsdl.sandbox.usuario');
                $this->password = Config::get('packages.codelko.ciberfactura.config.wsdl.sandbox.password');
            }

            if(!$this->endpoint){
                throw new CfdiException("No se ha definido la variable de entorno y/o configuraci�n para el endpoint del PAC Smarter Web.");
            }

            if(!$this->usuario){
                throw new CfdiException("No se ha definido la variable de entorno y/o configuraci�n para el usuario del PAC Smarter Web.");
            }

            if(!$this->password){
                throw new CfdiException("No se ha definido la variable de entorno y/o configuraci�n para el password del PAC Smarter Web.");
            }

            $this->rfc = $rfc;

            $params = [
                "url" => $this->endpoint,
                "user" => $this->usuario,
                "password"=> $this->password
            ];

            try{
                $auth = Authentication::auth($params);

                $token = $auth::Token();
                $token = $token->data;
                $token = isset($token->token) ? $token->token : '';

                $this->token = $token;
            }
            catch(\Exception $e){
                throw new CfdiException($e->getMessage());
            }

            $url_cer = $certificate["cer"];
            $url_key = $certificate["key"];
            $clave_privada = $certificate["password"];

            $this->cer = base64_encode(file_get_contents($url_cer));
            $this->key = base64_encode(file_get_contents($url_key));
            $this->clave_privada = $clave_privada;
        }

        public function timbrar($xml){
            $params = [
                "url" => $this->endpoint,
                "token" => $this->token
            ];

            try{
                $stamp = StampService::Set($params);

                $result = $stamp::StampV1($xml);

                if($result->status == "success"){
                    $response = $result->data->tfd;

                    $tmp1 = substr($response, strpos($response, 'xsi:schemaLocation="') + 20);
                    $tmp1 = substr($tmp1, 0, strpos($tmp1, '"'));

                    $tmp2 = substr($response, strpos($response, 'xmlns:tfd="') + 11);
                    $tmp2 = substr($tmp2, 0, strpos($tmp2, '"'));

                    $tmp3 = substr($response, strpos($response, 'xmlns:xsi="') + 11);
                    $tmp3 = substr($tmp3, 0, strpos($tmp3, '"'));

                    $timbre = simplexml_load_string($response);

                    $timbre[0]["xsi:schemaLocation"] = str_replace('"', '', $tmp1);
                    $timbre[0]["xmlns:tfd"] = str_replace('"', '', $tmp2);
                    $timbre[0]["xmlns:xsi"] = str_replace('"', '', $tmp3);

                    $this->stamp = new TimbreFiscalDigital(strtoupper($timbre[0]["UUID"].""), str_replace("T"," ", $timbre[0]["FechaTimbrado"].""), $timbre[0]["SelloCFD"]."", $timbre[0]["RfcProvCertif"]."", $timbre[0]["NoCertificadoSAT"]."", $timbre[0]["SelloSAT"]."");

                    return $this->stamp;
                }
                else{
                    throw new CfdiException($result->message);
                }
            }
            catch(\Exception $e){
                throw new CfdiException($e->getMessage());
            }
        }
    }