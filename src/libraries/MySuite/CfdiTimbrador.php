<?php
namespace Codelko\Ciberfactura\Libraries\MySuite;

use Codelko\Ciberfactura\Libraries\CfdiTimbradoInterface;

class CfdiTimbrador implements CfdiTimbradoInterface {
    protected $soap_client;
    protected $xml;

    public function __construct(){
        try{
            $this->soap_client = new \SoapClient("https://www.mysuitetest.com/mx.com.fact.wsfront/FactWSFront.asmx?wsdl");
        }
        catch (\Exception $e){
            dd($e);
        }
    }

    public function timbrar($xml){
        $this->xml = new \XMLWriter();

        $this->xml->openMemory();
        $this->xml->startElementNS(null, "RequestTransaction", NULL);
        // Send NULL to not specify the NameSpace on every call.
        //$this->xml->startElement('poliza');

        $this->addElement("Requestor",'69630b5d-c407-4d9a-bb5f-ff5fab4d764e');
        $this->addElement("Transaction","TIMBRAR");
        $this->addElement("Country","MX");
        $this->addElement("Entity","VECR8307073J1");
        $this->addElement("User","69630b5d-c407-4d9a-bb5f-ff5fab4d764e");
        $this->addElement("UserName","MX.VECR8307073J1.Ramiro");
        $this->addElement("Data1",base64_encode($xml));
        $this->addElement("Data2",'');
        $this->addElement("Data3", '');

        $args = $this->xml();

        try{
            $this->soap_client->RequestTransaction($args);
        }
        catch (\Exception $e){
            dd($e);
        }
    }

    public function addElement($name, $value){
        $value = (string) $value;

        $this->xml->startElement($name);

        $this->xml->Text($value);

        $this->xml->endElement();
    }

    public function xml(){
        $xml = $this->xml;

        $xml->endElement();
        $xml->endElement();

        $xml = $xml->outputMemory();

        return $xml;
    }
}