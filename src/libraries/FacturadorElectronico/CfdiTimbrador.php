<?php
namespace Codelko\Ciberfactura\Libraries\FacturadorElectronico;

use Codelko\Ciberfactura\Libraries\CfdiException;
use Codelko\Ciberfactura\Libraries\CfdiTimbradoInterface;
use Codelko\Ciberfactura\Libraries\Complementos\TimbreFiscalDigital;

class CfdiTimbrador implements CfdiTimbradoInterface {
    protected $soap_client;
    protected $xml;

    public function __construct(){
        try{
            $this->soap_client = new \SoapClient("https://timbradopruebas.stagefacturador.com/timbrado.asmx?WSDL");
        }
        catch (\Exception $e){
            dd($e);
        }
    }

    public function timbrar($xml){
        $parametros = array(
            'CFDIcliente' => $xml,
            'Usuario' => 'test',
            'password' => 'TEST'
        );

        try{
            $resultado = $this->soap_client->obtenerTimbrado($parametros);

            $xml = $resultado->obtenerTimbradoResult->any;

            $xml_object = simplexml_load_string($xml);

            if(strtolower($xml_object["esValido"]) == "true"){
                $xml = substr($xml, strpos($xml, '<tfd:TimbreFiscalDigital'));
                $xml = trim(substr($xml, 0, strpos($xml, '</timbre>')));

                dd($xml);
            }
            else{
                throw new CfdiException($xml_object->errores->Error["mensaje"]);
            }

            dd($xml);

            $xml = simplexml_load_string($xml);

            dd($xml);

            $this->stamp = new TimbreFiscalDigital(strtoupper($timbre[0]["UUID"]), str_replace("T"," ", $timbre[0]["FechaTimbrado"].""), $timbre[0]["SelloCFD"], $timbre[0]["RfcProvCertif"], $timbre[0]["NoCertificadoSAT"], $timbre[0]["SelloSAT"]);

            dd($resultado);
        }
        catch (\Exception $e){
            dd($e);
        }
    }

    public function addElement($name, $value){
        $value = (string) $value;

        $this->xml->startElement($name);

        $this->xml->Text($value);

        $this->xml->endElement();
    }

    public function xml(){
        $xml = $this->xml;

        $xml->endElement();
        $xml->endElement();

        $xml = $xml->outputMemory();

        return $xml;
    }
}