<?php
namespace Codelko\Ciberfactura\Libraries;

use Codelko\Ciberfactura\Models\CfdiTimbre as V33CfdiTimbre;
use Codelko\Ciberfactura\Models\V40\CfdiTimbre;

class Cfdi extends CfdiBase{
    public function __construct(){
        parent::__construct();
    }

    public function getInstance($production = false){
        $this->production = $production;
        return $this;
    }

    public function cadenaOriginal(){
        return $this->cadena_original;
    }

    public function sellar(){
        $xslt = __DIR__.'/../resources/xslt/cadenaoriginal_3_3.xslt';
        if($this->cfdi->version == "4.0"){
            $xslt = __DIR__.'/../resources/xslt/cadenaoriginal_4_0.xslt';
        }

        if(!file_exists(public_path()."/temp")){
            mkdir(public_path()."/temp");
        }

        $this->tmp_file = public_path()."/temp/".strtoupper(sha1(date("Y-m-d H:i:s".rand(0,100000)))).".xml";
        $this->xml->saveFile($this->tmp_file, false);

        $this->cadena_original = CfdiBase::getOriginalString($this->tmp_file, $xslt);

        $this->cfdi->cadena_original = $this->cadena_original;
        $this->cfdi->save();

        @unlink($this->tmp_file);

        $this->sello = $this->signData($this->key, $this->cadena_original);

        $this->cfdi->no_certificado = $this->no_certificado;
        $this->cfdi->certificado = $this->certificado;
        $this->cfdi->sello = $this->sello;
        $this->cfdi->save();

        $this->xml->sellar($this->sello, $this->no_certificado, $this->certificado);

        return $this->sello;
    }

    public function timbrar(){
        try {
            //Validación del XML que solo se aplicará a la versión 4.0
            if($this->cfdi->version == "4.0"){
                if($this->cfdi->tipo_de_comprobante != "N" && $this->cfdi->tipo_de_comprobante != "P"){
                    $xsd = __DIR__.'/../resources/xsd/cfdv40.xsd';

                    $this->tmp_file = public_path()."/temp/".strtoupper(sha1(date("Y-m-d H:i:s".rand(0,100000)))).".xml";
                    $this->xml->saveFile($this->tmp_file, false);
                    
                    CfdiBase::xmlValidation($this->tmp_file, $xsd);

                    @unlink($this->tmp_file);
                }
            }

            $timbre = $this->timbrador->timbrar($this->xml->getXML());

            if(!$timbre || !is_object($timbre) || get_class($timbre) != 'Codelko\\Ciberfactura\\Libraries\\Complementos\\TimbreFiscalDigital'){
                throw new CfdiException("El timbrador tiene que ser un objeto de la clase Codelko\\Ciberfactura\\Complementos\\TimbreFiscalDigital");
            }

            $data = [
                'cfdi_id' => $this->cfdi->id,
                'version' => $timbre->version,
                'uuid' => $timbre->uuid,
                'fecha_timbrado' => str_replace("T"," ", $timbre->fecha_timbrado),
                'rfc_pac' => $timbre->rfc_pac,
                'sello_cfd' => $timbre->sello_cfd,
                'no_certificado_sat' => $timbre->no_certificado,
                'sello_sat' => $timbre->sello_sat,
                'cadena_original' => $timbre->getOriginalString()
            ];

            if($this->cfdi->version == "4.0"){
                CfdiTimbre::create($data);
            }
            else{
                V33CfdiTimbre::create($data);
            }

            $this->xml->timbrar($timbre);

            $this->cfdi->uuid = $timbre->uuid;
            $this->cfdi->save();

            $this->guardar();

            return $timbre->uuid;
        }
        catch(\Exception $e){
            throw $e;

            return;
        }
    }

    public function cancelar($motivo, $folio_sustitucion = false){
        try{
            return $this->cancelador->cancelar($this->cfdi->uuid, $motivo, $folio_sustitucion);
        }
        catch(CfdiException $e){
            throw $e;
        }
    }

    public function guardar($path = false){
        if(!$path){
            $this->xml->completeFile(public_path()."/cfdis/".strtoupper($this->cfdi->uuid).".xml");
        }
        else{
            $this->xml->completeFile($path);
            $this->xml->completeFile(public_path()."/cfdis/".strtoupper($this->cfdi->uuid).".xml");
        }

        return $this->cfdi;
    }

    public function complementar(CfdiComplemento $complemento){
        $this->xml->complementar($complemento);

        $xslt = __DIR__.'/../resources/xslt/cadenaoriginal_3_3.xslt';
        if($this->cfdi->version == "4.0"){
            $xslt = __DIR__.'/../resources/xslt/cadenaoriginal_4_0.xslt';
        }

        if(!file_exists(public_path()."/temp")){
            mkdir(public_path()."/temp");
        }

        $this->tmp_file = public_path()."/temp/".strtoupper(sha1(date("Y-m-d H:i:s".rand(0,100000)))).".xml";
        $this->xml->saveFile($this->tmp_file, false);

        $this->cadena_original = CfdiBase::getOriginalString($this->tmp_file, $xslt);

        @unlink($this->tmp_file);
    }

    public function addendar(){
        $this->xml->addendar();
    }

}