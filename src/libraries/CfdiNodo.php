<?php
    namespace Codelko\Ciberfactura\Libraries;

    class CfdiNodo{
        public $nombre;
        public $value;
        public $atributos;
        public $nodos;

        public function __construct($nombre, $value = ''){
            $this->nombre = $nombre;
            $this->value = $value;
            $this->atributos = array();
            $this->nodos = array();
        }

        public function agregarAtributo($nombre, $valor){
            $valor = str_replace("&", "&amp;", $valor);
            //$valor = str_replace("'", "&quot;", $valor);
            $valor = str_replace('"', "&apos;", $valor);
            $valor = str_replace(">", "&lt;", $valor);
            $valor = str_replace("<", "&gt;", $valor);
            $valor = str_replace("|", " ", $valor);

            $this->atributos[$nombre] = $valor;
        }

        public function agregarNodo($nombre){
            $nombre->nombre = str_replace("&", "&amp;", $nombre->nombre);
            //$nombre->nombre = str_replace("'", "&quot;", $nombre->nombre);
            $nombre->nombre = str_replace('"', "&apos;", $nombre->nombre);
            $nombre->nombre = str_replace(">", "&lt;", $nombre->nombre);
            $nombre->nombre = str_replace("<", "&gt;", $nombre->nombre);
            $nombre->nombre = str_replace("|", " ", $nombre->nombre);

            $this->nodos[] = $nombre;
        }

        public function tieneAtributos(){
            if(count($this->atributos) > 0) return true;
            return false;
        }

        public function tieneNodos(){
            if(count($this->nodos) > 0) return true;
            return false;
        }
    }