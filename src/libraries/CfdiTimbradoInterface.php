<?php

    namespace Codelko\Ciberfactura\Libraries;

    interface CfdiTimbradoInterface{
        public function timbrar($xml);
    }