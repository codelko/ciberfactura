<?php

    namespace Codelko\Ciberfactura\Libraries;

    interface CfdiCanceladoInterface{
        public function cancelar($xml, $motivo, $folio_sustitucion = false);
    }