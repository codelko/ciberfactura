<?php

namespace Codelko\Ciberfactura\Libraries;

abstract class CfdiComplemento{
    public $xml;

    abstract function getNodo();
    abstract function getNS();
    abstract function getSchemaLocation();

    public function getXML(){
        $nodo = $this->getNodo();

        $nodo->agregarAtributo("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
        $nodo->agregarAtributo("xsi:schemaLocation", implode(" ", $this->getSchemaLocation()));

        $nss = $this->getNS();
        if($nss) foreach($nss as $ns => $xmlns){
            $nodo->agregarAtributo($ns, $xmlns);
        }

        $this->xml = new \DOMDocument('1.0' , 'UTF-8');
        $this->xml->appendChild($this->xmlizar($nodo));
        $this->xml->formatOutput = true;
        return $this->xml->saveXML();
    }

    public function xmlizar($nodo){
        $xml = $this->xml->createElement($nodo->nombre);

        if($nodo->atributos) foreach($nodo->atributos as $nombre => $valor){
            if($valor) {
                $tmp = $this->xml->createAttribute($nombre);
                $tmp->value = $valor;
                $xml->appendChild($tmp);
            }
        }

        if($nodo->nodos) foreach($nodo->nodos as $n){
            $tmp = $this->xmlizar($n);
            $xml->appendChild($tmp);
        }

        return $xml;
    }
}