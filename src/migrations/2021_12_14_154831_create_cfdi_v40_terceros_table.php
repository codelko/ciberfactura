<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCfdiV40TercerosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cfdi_v40_terceros', function (Blueprint $table) {
            $table->id();

            $table->unsignedInteger('cfdi_id');
            $table->unsignedInteger('cfdi_concepto_id');

            $table->string('rfc',13);
            $table->string('nombre');
            $table->string('regimen_fiscal');
            $table->string('domicilio_fiscal');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cfdi_v40_terceros');
    }
}
