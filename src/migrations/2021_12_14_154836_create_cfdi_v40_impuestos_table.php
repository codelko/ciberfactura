<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCfdiV40ImpuestosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cfdi_v40_impuestos', function (Blueprint $table) {
            $table->id();
            
            $table->unsignedInteger('cfdi_id');
            $table->unsignedInteger('cfdi_concepto_id');

            $table->string('type')->default("traslado");

            $table->decimal('base',10,2);
            $table->string('impuesto');
            $table->string('tipo_factor');
            $table->decimal('tasa_o_cuota',10,6)->default(0.00);
            $table->decimal('importe',10,2)->default(0.00);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cfdi_v40_impuestos');
    }
}
