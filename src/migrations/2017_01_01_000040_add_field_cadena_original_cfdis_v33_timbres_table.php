<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldCadenaOriginalCfdisV33TimbresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cfdi_v33_timbres', function($table){
            $table->text('cadena_original')->nullable()->after("no_certificado_sat");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cfdi_v33_facturas', function($table){
            $table->dropColumn('cadena_original');
        });
    }
}
