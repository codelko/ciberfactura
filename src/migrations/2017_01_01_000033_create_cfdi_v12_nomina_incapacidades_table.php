<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCfdiV12NominaIncapacidadesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cfdi_nomina_v12_incapacidades', function(Blueprint $table)
		{
			$table->increments('id');

			$table->integer('account_id')->unsigned();
			$table->integer('cfdi_id')->unsigned();
			$table->integer('nomina_id')->unsigned();

			$table->integer('dias')->nullable();
			$table->string('tipo')->default("")->nullable();
			$table->decimal('descuento',10,4)->nullable();

			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('cfdi_nomina_v12_incapacidades');
	}

}
