<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateCfdiV40PagosRelacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cfdi_v40_pagos_relaciones', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');

            $table->unsignedInteger('cfdi_id');
            $table->unsignedInteger('cfdi_pago_id');

            $table->string('id_documento');
            $table->string('serie')->nullable()->default("");
            $table->string('folio')->nullable()->default("");

            $table->string('moneda')->default("MXN");
            $table->string('tipo_cambio')->nullable()->default("");
            $table->string('metodo_pago');

            $table->integer('num_parcialidad');
            $table->decimal('importe_saldo_anterior', 12, 2);
            $table->decimal('importe_pagado', 12, 2);
            $table->decimal('importe_saldo_insoluto', 12, 2);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cfdi_v40_pagos_relaciones');
    }
}
