<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCfdiV40FacturasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cfdi_v40_facturas', function (Blueprint $table) {
            $table->id();

            //Información General del CFDI
            $table->string('version')->default("4.0");
            $table->string('serie',25)->default("");
            $table->string('folio',20)->default("");
            $table->dateTime('fecha');
            $table->text('sello')->nullable();
            $table->string('forma_pago')->nullable()->default("01");
            $table->string('no_certificado',20)->nullable()->default("");
            $table->text('certificado')->nullable()->default("");
            $table->string('condiciones_de_pago')->nullable();
            $table->decimal('sub_total', 10, 2);
            $table->decimal('descuento', 10, 2)->default(0.00);
            $table->string('moneda')->nullable();
            $table->string('tipo_cambio')->nullable()->default("");
            $table->decimal('total', 10, 2);
            $table->string('tipo_de_comprobante')->default("I");
            $table->string('exportacion')->default("01");
            $table->string('metodo_pago')->nullable()->default("PUE");
            $table->string('lugar_expedicion')->nullable()->default("");
            $table->string('confirmacion', 5)->nullable()->default("");
            $table->string('estado')->default("EMITIDA");

            //Información para Factura Global
            $table->string('periodicidad')->nullable()->default("");
            $table->string('meses')->nullable()->default("");
            $table->unsignedInteger('ejercicio')->nullable()->default(2021);

            //Información del Timbre Fiscal
            $table->string('uuid')->nullable()->default("");
            $table->text('cadena_original')->nullable();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cfdi_v40_facturas');
    }
}
