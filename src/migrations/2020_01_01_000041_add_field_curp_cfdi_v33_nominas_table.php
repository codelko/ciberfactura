<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;

class AddFieldCurpCfdiV33NominasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cfdi_nomina_v12_nominas', function($table){
            $table->string('curp_patronal',25)->nullable()->default("")->after("registro_patronal");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cfdi_nomina_v12_nominas', function($table){
            $table->dropColumn('curp_patronal');
        });
    }
}
