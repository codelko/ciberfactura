<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCfdiV12NominasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cfdi_nomina_v12_nominas', function(Blueprint $table)
		{
			$table->increments('id');

			$table->integer('account_id')->unsigned();
			$table->integer('cfdi_id')->unsigned();

			$table->string('version')->default("1.1");
			$table->string('registro_patronal')->nullable();
			$table->string('numero_empleado')->nullable();
			$table->string('rfc',13)->nullable();
			$table->string('curp',50)->nullable();

			$table->string('tipo_regimen')->nullable();
			$table->string('nss')->nullable();
			$table->date('fecha_pago')->nullable();
			$table->date('fecha_inicial')->nullable();
			$table->date('fecha_final')->nullable();
			$table->integer('dias_trabajados')->nullable();
			$table->string('clabe')->nullable();
			$table->string('banco')->nullable();
			$table->date('fecha_contratacion')->nullable();
			$table->string('antiguedad',25)->nullable();
			$table->string('puesto')->nullable();
			$table->string('tipo_contrato')->nullable();
			$table->string('tipo_jornada')->nullable();
			$table->string('periodicidad')->nullable();
			$table->string('riesgo_puesto')->nullable();
			$table->decimal('salario_base', 10,4)->nullable();
			$table->decimal('salario_integrado', 10,4)->nullable();

			$table->string('estado')->default("EMITIDA");

			$table->string('xml')->nullable();
			$table->string('pdf')->nullable();

			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('cfdi_nomina_v12_nominas');
	}

}
