<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCfdisV33PagosRelacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cfdi_v33_pagos_relaciones', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');

            $table->unsignedInteger('cfdi_id');
            $table->unsignedInteger('cfdi_pago_id');

            $table->string('id_documento');
            $table->string('serie')->nullable()->default("");
            $table->string('folio')->nullable()->default("");

            $table->string('moneda')->default("MXN");
            $table->string('tipo_cambio')->nullable()->default("");
            $table->string('metodo_pago');

            $table->integer('num_parcialidad');
            $table->decimal('importe_saldo_anterior', 10, 2);
            $table->decimal('importe_pagado', 10, 2);
            $table->decimal('importe_saldo_insoluto', 10, 2);

            $table->timestamps();

            $table->foreign('cfdi_id')->references('id')->on('cfdi_v33_facturas')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('cfdi_pago_id')->references('id')->on('cfdi_v33_pagos')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cfdi_v33_pagos_relaciones');
    }
}
