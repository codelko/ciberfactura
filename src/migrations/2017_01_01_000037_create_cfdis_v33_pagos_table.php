<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCfdisV33PagosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cfdi_v33_pagos', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');

            $table->unsignedInteger('cfdi_id');

            $table->dateTime('fecha_pago');
            $table->string('forma_pago');
            $table->string('moneda')->default("MXN");
            $table->string('tipo_cambio')->nullable()->default("");
            $table->decimal('monto', 10, 2);
            $table->string('num_operacion')->nullable()->default("");

            $table->string('rfc_emisor_cta_ord')->nullable()->default("");
            $table->string('nom_banco_ord_ext')->nullable()->default("");
            $table->string('cta_ordenante')->nullable()->default("");
            $table->string('rfc_emisor_cta_ben')->nullable()->default("");
            $table->string('cta_beneficiario')->nullable()->default("");

            $table->string('tipo_cad_pago')->nullable()->default("");
            $table->string('cert_pago')->nullable()->default("");
            $table->string('cad_pago')->nullable()->default("");
            $table->string('sello_pago')->nullable()->default("");

            $table->timestamps();

            $table->foreign('cfdi_id')->references('id')->on('cfdi_v33_facturas')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cfdi_v33_pagos');
    }
}
