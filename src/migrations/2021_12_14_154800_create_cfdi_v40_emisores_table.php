<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCfdiV40EmisoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cfdi_v40_emisores', function (Blueprint $table) {
            $table->id();

            $table->unsignedInteger('cfdi_id');

            $table->string('rfc',13);
            $table->string('nombre');
            $table->string('regimen_fiscal');
            $table->string('fac_atr_adquirente')->nullable()->default("");

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cfdi_v40_emisores');
    }
}
