<?php

use Codelko\Ciberfactura\Models\Catalogs\CfdiSegmentoProducto;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCfdiV33CatSegmentosProductosServiciosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cfdi_v33_cat_segmentos_productos_servicios', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';

            $table->increments('id');

            $table->unsignedInteger('type_id');

            $table->string('name');

            $table->timestamps();

            $table->foreign('type_id')->references('id')->on('cfdi_v33_cat_tipos_productos_servicios')->onUpdate('cascade')->onDelete('cascade');
        });

        CfdiSegmentoProducto::create(['id' => 1, 'type_id' => 1,'name' => 'Alimentos, Bebidas y Tabaco']);
        CfdiSegmentoProducto::create(['id' => 2, 'type_id' => 1,'name' => 'Artículos Domésticos, Suministros y Productos Electrónicos de Consumo']);
        CfdiSegmentoProducto::create(['id' => 3, 'type_id' => 1,'name' => 'Componentes y Equipos para Distribución y Sistemas de Acondicionamiento']);
        CfdiSegmentoProducto::create(['id' => 4, 'type_id' => 1,'name' => 'Componentes y Suministros de Manufactura']);
        CfdiSegmentoProducto::create(['id' => 5, 'type_id' => 1,'name' => 'Componentes y Suministros Electrónicos']);
        CfdiSegmentoProducto::create(['id' => 6, 'type_id' => 1,'name' => 'Componentes y Suministros para Estructuras, Edificación, Construcción y Obras Civiles']);
        CfdiSegmentoProducto::create(['id' => 7, 'type_id' => 1,'name' => 'Componentes, Accesorios y Suministros de Sistemas Eléctricos e Iluminación']);
        CfdiSegmentoProducto::create(['id' => 8, 'type_id' => 1,'name' => 'Difusión de Tecnologías de Información y Telecomunicaciones']);
        CfdiSegmentoProducto::create(['id' => 9, 'type_id' => 1,'name' => 'Equipo Médico, Accesorios y Suministros']);
        CfdiSegmentoProducto::create(['id' => 10, 'type_id' => 1,'name' => 'Equipos de Limpieza y Suministros']);
        CfdiSegmentoProducto::create(['id' => 11, 'type_id' => 1,'name' => 'Equipos de Oficina, Accesorios y Suministros']);
        CfdiSegmentoProducto::create(['id' => 12, 'type_id' => 1,'name' => 'Equipos y Suministros de Defensa, Orden Publico, Proteccion, Vigilancia y Seguridad']);
        CfdiSegmentoProducto::create(['id' => 13, 'type_id' => 1,'name' => 'Equipos y Suministros de Laboratorio, de Medición, de Observación y de Pruebas']);
        CfdiSegmentoProducto::create(['id' => 14, 'type_id' => 1,'name' => 'Equipos y Suministros para Impresión, Fotografia y Audiovisuales']);
        CfdiSegmentoProducto::create(['id' => 15, 'type_id' => 1,'name' => 'Equipos, Suministros y Accesorios para Deportes y Recreación']);
        CfdiSegmentoProducto::create(['id' => 16, 'type_id' => 1,'name' => 'Herramientas y Maquinaria General']);
        CfdiSegmentoProducto::create(['id' => 17, 'type_id' => 1,'name' => 'Instrumentos Musicales, Juegos, Juguetes, Artes, Artesanías y Equipo educativo, Materiales, Accesorios y Suministros']);
        CfdiSegmentoProducto::create(['id' => 18, 'type_id' => 1,'name' => 'Maquinaria y Accesorios de Minería y Perforación de Pozos']);
        CfdiSegmentoProducto::create(['id' => 19, 'type_id' => 1,'name' => 'Maquinaria y Accesorios para Agricultura, Pesca, Silvicultura y Fauna']);
        CfdiSegmentoProducto::create(['id' => 20, 'type_id' => 1,'name' => 'Maquinaria y Accesorios para Construcción y Edificación']);
        CfdiSegmentoProducto::create(['id' => 21, 'type_id' => 1,'name' => 'Maquinaria y Accesorios para Generación y Distribución de Energía']);
        CfdiSegmentoProducto::create(['id' => 22, 'type_id' => 1,'name' => 'Maquinaria y Accesorios para Manufactura y Procesamiento Industrial']);
        CfdiSegmentoProducto::create(['id' => 23, 'type_id' => 1,'name' => 'Maquinaria, Accesorios y Suministros para Manejo, Acondicionamiento y Almacenamiento de Materiales']);
        CfdiSegmentoProducto::create(['id' => 24, 'type_id' => 1,'name' => 'Maquinaria, Equipo y Suministros para la Industria de Servicios']);
        CfdiSegmentoProducto::create(['id' => 25, 'type_id' => 1,'name' => 'Material Mineral, Textil y Vegetal y Animal No Comestible']);
        CfdiSegmentoProducto::create(['id' => 26, 'type_id' => 1,'name' => 'Material Químico incluyendo Bioquímicos y Materiales de Gas']);
        CfdiSegmentoProducto::create(['id' => 27, 'type_id' => 1,'name' => 'Material Vivo Vegetal y Animal, Accesorios y Suministros']);
        CfdiSegmentoProducto::create(['id' => 28, 'type_id' => 1,'name' => 'Materiales Combustibles, Aditivos para Combustibles, Lubricantes y Anticorrosivos']);
        CfdiSegmentoProducto::create(['id' => 29, 'type_id' => 1,'name' => 'Materiales de Resina, Colofonia, Caucho, Espuma, Película y Elastómericos']);
        CfdiSegmentoProducto::create(['id' => 30, 'type_id' => 1,'name' => 'Materiales y Productos de Papel']);
        CfdiSegmentoProducto::create(['id' => 31, 'type_id' => 1,'name' => 'Medicamentos y Productos Farmacéuticos']);
        CfdiSegmentoProducto::create(['id' => 32, 'type_id' => 1,'name' => 'Muebles, Mobiliario y Decoración']);
        CfdiSegmentoProducto::create(['id' => 33, 'type_id' => 2,'name' => 'Organizaciones y Clubes']);
        CfdiSegmentoProducto::create(['id' => 34, 'type_id' => 1,'name' => 'Productos para Relojería, Joyería y Piedras Preciosas']);
        CfdiSegmentoProducto::create(['id' => 35, 'type_id' => 1,'name' => 'Publicaciones Impresas, Publicaciones Electrónicas y Accesorios']);
        CfdiSegmentoProducto::create(['id' => 36, 'type_id' => 1,'name' => 'Ropa, Maletas y Productos de Aseo Personal']);
        CfdiSegmentoProducto::create(['id' => 37, 'type_id' => 2,'name' => 'Servicios Basados en Ingeniería, Investigación y Tecnología']);
        CfdiSegmentoProducto::create(['id' => 38, 'type_id' => 2,'name' => 'Servicios de Contratación Agrícola, Pesquera, Forestal y de Fauna']);
        CfdiSegmentoProducto::create(['id' => 39, 'type_id' => 2,'name' => 'Servicios de Defensa Nacional, Orden Publico, Seguridad y Vigilancia']);
        CfdiSegmentoProducto::create(['id' => 40, 'type_id' => 2,'name' => 'Servicios de Edificación, Construcción de Instalaciones y Mantenimiento']);
        CfdiSegmentoProducto::create(['id' => 41, 'type_id' => 2,'name' => 'Servicios de Gestión, Servicios Profesionales de Empresa y Servicios Administrativos']);
        CfdiSegmentoProducto::create(['id' => 42, 'type_id' => 2,'name' => 'Servicios de Limpieza, Descontaminación y Tratamiento de Residuos']);
        CfdiSegmentoProducto::create(['id' => 43, 'type_id' => 2,'name' => 'Servicios de Minería, Petróleo y Gas']);
        CfdiSegmentoProducto::create(['id' => 44, 'type_id' => 2,'name' => 'Servicios de Producción Industrial y Manufactura']);
        CfdiSegmentoProducto::create(['id' => 45, 'type_id' => 2,'name' => 'Servicios de Salud']);
        CfdiSegmentoProducto::create(['id' => 46, 'type_id' => 2,'name' => 'Servicios de Transporte, Almacenaje y Correo']);
        CfdiSegmentoProducto::create(['id' => 47, 'type_id' => 2,'name' => 'Servicios de Viajes, Alimentación, Alojamiento y Entretenimiento']);
        CfdiSegmentoProducto::create(['id' => 48, 'type_id' => 2,'name' => 'Servicios Editoriales, de Diseño, de Artes Graficas y Bellas Artes']);
        CfdiSegmentoProducto::create(['id' => 49, 'type_id' => 2,'name' => 'Servicios Educativos y de Formación']);
        CfdiSegmentoProducto::create(['id' => 50, 'type_id' => 2,'name' => 'Servicios Financieros y de Seguros']);
        CfdiSegmentoProducto::create(['id' => 51, 'type_id' => 2,'name' => 'Servicios Medioambientales']);
        CfdiSegmentoProducto::create(['id' => 52, 'type_id' => 2,'name' => 'Servicios Personales y Domésticos']);
        CfdiSegmentoProducto::create(['id' => 53, 'type_id' => 2,'name' => 'Servicios Políticos y de Asuntos Cívicos']);
        CfdiSegmentoProducto::create(['id' => 54, 'type_id' => 2,'name' => 'Servicios Públicos y Servicios Relacionados con el Sector Público']);
        CfdiSegmentoProducto::create(['id' => 55, 'type_id' => 1,'name' => 'Terrenos, Edificios, Estructuras y Vías']);
        CfdiSegmentoProducto::create(['id' => 56, 'type_id' => 1,'name' => 'Vehículos Comerciales, Militares y Particulares, Accesorios y Componentes']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cfdi_v33_cat_segmentos_productos_servicios');
    }
}
