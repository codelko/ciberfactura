<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Codelko\Ciberfactura\Models\Catalogs\CfdiUso;

class CreateCfdisV33CatalogoUsosCfdiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cfdi_v33_cat_usos_cfdi', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';

            $table->increments('id');

            $table->string('code');
            $table->string('name');
            $table->boolean('fisica')->default(true);
            $table->boolean('moral')->default(true);

            $table->timestamps();
        });

        CfdiUso::create(["code" => "G01", "name" => "Adquisición de mercancias", "fisica" => true, "moral" => true]);
        CfdiUso::create(["code" => "G02", "name" => "Devoluciones, descuentos o bonificaciones", "fisica" => true, "moral" => true]);
        CfdiUso::create(["code" => "G03", "name" => "Gastos en general", "fisica" => true, "moral" => true]);
        CfdiUso::create(["code" => "I01", "name" => "Construcciones", "fisica" => true, "moral" => true]);
        CfdiUso::create(["code" => "I02", "name" => "Mobilario y equipo de oficina por inversiones", "fisica" => true, "moral" => true]);
        CfdiUso::create(["code" => "I03", "name" => "Equipo de transporte", "fisica" => true, "moral" => true]);
        CfdiUso::create(["code" => "I04", "name" => "Equipo de computo y accesorios", "fisica" => true, "moral" => true]);
        CfdiUso::create(["code" => "I05", "name" => "Dados, troqueles, moldes, matrices y herramental", "fisica" => true, "moral" => true]);
        CfdiUso::create(["code" => "I06", "name" => "Comunicaciones telefónicas", "fisica" => true, "moral" => true]);
        CfdiUso::create(["code" => "I07", "name" => "Comunicaciones satelitales", "fisica" => true, "moral" => true]);
        CfdiUso::create(["code" => "I08", "name" => "Otra maquinaria y equipo", "fisica" => true, "moral" => true]);
        CfdiUso::create(["code" => "D01", "name" => "Honorarios médicos, dentales y gastos hospitalarios.", "fisica" => true, "moral" => false]);
        CfdiUso::create(["code" => "D02", "name" => "Gastos médicos por incapacidad o discapacidad", "fisica" => true, "moral" => false]);
        CfdiUso::create(["code" => "D03", "name" => "Gastos funerales", "fisica" => true, "moral" => false]);
        CfdiUso::create(["code" => "D04", "name" => "Donativos", "fisica" => true, "moral" => false]);
        CfdiUso::create(["code" => "D05", "name" => "Intereses reales efectivamente pagados por créditos hipotecarios (casa habitación)", "fisica" => true, "moral" => false]);
        CfdiUso::create(["code" => "D06", "name" => "Aportaciones voluntarias al SAR", "fisica" => true, "moral" => false]);
        CfdiUso::create(["code" => "D07", "name" => "Primas por seguros de gastos médicos", "fisica" => true, "moral" => false]);
        CfdiUso::create(["code" => "D08", "name" => "Gastos de transportación escolar obligatoria", "fisica" => true, "moral" => false]);
        CfdiUso::create(["code" => "D09", "name" => "Depósitos en cuentas para el ahorro, primas que tengan como base planes de pensiones", "fisica" => true, "moral" => false]);
        CfdiUso::create(["code" => "D10", "name" => "Pagos por servicios educativos (colegiaturas)", "fisica" => true, "moral" => false]);
        CfdiUso::create(["code" => "P01", "name" => "Por definir", "fisica" => true, "moral" => true]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cfdi_v33_cat_usos_cfdi');
    }
}
