<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCfdiV40ReceptoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cfdi_v40_receptores', function (Blueprint $table) {
            $table->id();
            
            $table->unsignedInteger('cfdi_id');

            $table->string('rfc',13);
            $table->string('nombre');
            $table->string('domicilio_fiscal');
            $table->string('regimen_fiscal');
            $table->string('residencia_fiscal')->nullable()->default("MEX");
            $table->string('num_reg_id_trib')->nullable()->default("");
            $table->string('uso_cfdi')->nullable()->default("P01");

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cfdi_v40_receptores');
    }
}
