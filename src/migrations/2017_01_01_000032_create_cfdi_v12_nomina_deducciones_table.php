<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCfdiV12NominaDeduccionesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cfdi_nomina_v12_deducciones', function(Blueprint $table)
		{
			$table->increments('id');

			$table->integer('account_id')->unsigned();
			$table->integer('cfdi_id')->unsigned();
			$table->integer('nomina_id')->unsigned();

			$table->string('tipo')->default("")->nullable();
			$table->string('clave')->default("")->nullable();
			$table->text('concepto')->nullable();
			$table->decimal('importe_gravado',10,4)->nullable();
			$table->decimal('importe_exento',10,4)->nullable();

			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('cfdi_nomina_v12_deducciones');
	}

}
