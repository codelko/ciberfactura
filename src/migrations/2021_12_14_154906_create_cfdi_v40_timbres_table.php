<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCfdiV40TimbresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cfdi_v40_timbres', function (Blueprint $table) {
            $table->id();

            $table->unsignedInteger('cfdi_id');

            $table->string('version');
            $table->string('uuid');
            $table->datetime('fecha_timbrado');
            $table->string('rfc_pac');
            $table->text('sello_cfd');
            $table->string('no_certificado_sat');
            $table->text('cadena_original');
            $table->text('sello_sat');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cfdi_v40_timbres');
    }
}
