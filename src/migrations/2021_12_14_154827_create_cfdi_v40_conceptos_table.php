<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCfdiV40ConceptosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cfdi_v40_conceptos', function (Blueprint $table) {
            $table->id();

            $table->unsignedInteger('cfdi_id');

            $table->string('clave_prod_serv');
            $table->string('no_identificacion')->nullable()->default("");
            $table->decimal('cantidad',14,6);
            $table->string('clave_unidad');
            $table->string('unidad')->nullable()->default("");
            $table->text('descripcion');
            $table->decimal('valor_unitario',10,2);
            $table->decimal('importe',10,2);
            $table->decimal('descuento',10,2);
            $table->string('objeto_imp')->nullable()->default("02");

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cfdi_v40_conceptos');
    }
}
