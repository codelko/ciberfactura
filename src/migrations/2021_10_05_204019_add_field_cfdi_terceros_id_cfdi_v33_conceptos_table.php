<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldCfdiTercerosIdCfdiV33ConceptosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cfdi_v33_conceptos', function($table){
            $table->unsignedInteger('cfdi_concepto_terceros_id')->nullable()->default(0)->after("cfdi_id");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cfdi_v33_conceptos', function($table){
            $table->dropColumn('cfdi_concepto_terceros_id');
        });
    }
}
