<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldEstadoCfdiV33FacturasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cfdi_v33_facturas', function($table){
            $table->string('estado')->nullable()->default("EMITIDA")->after("cadena_original");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cfdi_v33_facturas', function($table){
            $table->dropColumn('estado');
        });
    }
}
