<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateCfdiV40PagosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cfdi_v40_pagos', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');

            $table->unsignedInteger('cfdi_id');

            $table->dateTime('fecha_pago');
            $table->string('forma_pago');
            $table->string('moneda')->default("MXN");
            $table->string('tipo_cambio')->nullable()->default("");
            $table->decimal('monto', 12, 2);
            $table->string('num_operacion')->nullable()->default("");

            $table->string('rfc_emisor_cta_ord')->nullable()->default("");
            $table->string('nom_banco_ord_ext')->nullable()->default("");
            $table->string('cta_ordenante')->nullable()->default("");
            $table->string('rfc_emisor_cta_ben')->nullable()->default("");
            $table->string('cta_beneficiario')->nullable()->default("");

            $table->string('tipo_cad_pago')->nullable()->default("");
            $table->string('cert_pago')->nullable()->default("");
            $table->string('cad_pago')->nullable()->default("");
            $table->string('sello_pago')->nullable()->default("");

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cfdi_v40_pagos');
    }
}
