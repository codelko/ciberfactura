<?php

use Codelko\Ciberfactura\Models\Catalogs\CfdiTipoProducto;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCfdiV33CatTiposProductosServiciosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cfdi_v33_cat_tipos_productos_servicios', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';

            $table->increments('id');

            $table->string('name');

            $table->timestamps();
        });

        CfdiTipoProducto::create(['id' => 1,'name' => 'Productos']);
        CfdiTipoProducto::create(['id' => 2,'name' => 'Servicios']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cfdi_v33_cat_tipos_productos_servicios');
    }
}